/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.REST.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;

/**
 *
 * @author 1hlui
 */
public class DTOEnvio {

    enum Estado {
        EnTransito,
        EnReparto,
        Entregado,
        Extraviado
    }

    private int peso;

    private int dim;

    private int localizador;

    private float importe;

    private String destinatario;

    private String remitente;

    private Estado estado;

    @JsonFormat
    private LocalDateTime entrega;

    public DTOEnvio(int peso, int dim, String remitente, String destinatario) {
        this.peso = peso;
        this.dim = dim;
        this.destinatario = destinatario;
        this.remitente = remitente;
    }

    public DTOEnvio(int peso, int dim, int localizador, float importe, String remitente, String destinatario, Estado estado, LocalDateTime entrega) {
        this.peso = peso;
        this.dim = dim;
        this.localizador = localizador;
        this.importe = importe;
        this.destinatario = destinatario;
        this.remitente = remitente;
        this.estado = estado;
        this.entrega = entrega;
    }

//    public DTOEnvio(Envio envio) {
//        this.peso = envio.getPeso();
//        this.dim = envio.getDim();
//        this.localizador = envio.getLocalizador();
//        this.importe = envio.getImporte();
//        this.remitente = envio.getRemitente().getDni();
//        this.destinatario = envio.getDestinatario().getDni();
//        this.estado = envio.getEstado();
//        this.entrega = envio.getEntrega();
//    }
    public DTOEnvio() {
    }

    public DTOEnvio(int localizador, Estado estado, LocalDateTime entrega) {
        this.localizador = localizador;
        this.estado = estado;
        this.entrega = entrega;
    }

    /**
     * @return the peso
     */
    public int getPeso() {
        return peso;
    }

    /**
     * @return the dim
     */
    public int getDim() {
        return dim;
    }

    /**
     * @return the localizador
     */
    public int getLocalizador() {
        return localizador;
    }

    /**
     * @return the importe
     */
    public float getImporte() {
        return importe;
    }

    public String getDestinatario() {
        return destinatario;
    }

    public String getRemitente() {
        return remitente;
    }

    public Estado getEstado() {
        return estado;
    }

//    public Envio cEnvio() {
//        return new Envio(peso, dim, remitente, destinatario);
//    }
    /**
     * @return the entrega
     */
    public LocalDateTime getEntrega() {
        return entrega;
    }
    
    public String stringEstado() {
        return estado.toString();
    }
}
