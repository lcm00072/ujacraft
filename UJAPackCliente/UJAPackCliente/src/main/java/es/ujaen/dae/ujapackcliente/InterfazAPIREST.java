/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujapackcliente;

import es.ujaen.dae.REST.DTO.DTOAccionEnvio;
import es.ujaen.dae.REST.DTO.DTOAccionEnvio.Accion;
import es.ujaen.dae.REST.DTO.DTOEnvio;
import es.ujaen.dae.REST.DTO.DTOEtapa;
import es.ujaen.dae.REST.DTO.DTOPersona;
import es.ujaen.dae.interfazcliente.JFrameNotificacion;
import es.ujaen.dae.interfazcliente.JFrameNotificacion.TipoNotificacion;
import java.net.URI;
import java.time.LocalDateTime;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Diego
 */
@Component
public class InterfazAPIREST {

    /* Tipo de usuario que se puede logear */
    enum Tipo {
        cliente,
        administrador,
        repartidor
    }

    RestTemplate restTemplate;

    private String usuario;

    @LocalServerPort
    int localPort;


    RestTemplateBuilder restTemplateBuilder;

    /* Direccion principal */
    String ujapack = "http://localhost:8080/ujapack";
    

    /**
     * Constructor con un usuario y su contraseña
     *
     * @param usuario nombre que se ingresa en la ventana
     * @param pass contraseña que se ingresa
     */
    public InterfazAPIREST(String usuario, String pass) {
        
        //Guardamos el usuario
        this.usuario = usuario;
     
        restTemplateBuilder = new RestTemplateBuilder();

        /* Se comprueba la autentificacion */
        restTemplateBuilder = restTemplateBuilder.basicAuthentication(usuario, pass);

        restTemplate = new RestTemplate();
        restTemplate = restTemplateBuilder.build();

    }

    /**
     * Comprobamos la conexion a través de un GET en la direccion establecida en el servidor
     * para las comprobaciones
     *
     * @return devuelve con un boolean si el usuario tiene permisos para acceder al servidor
     */
    public boolean comprobarConexion() {

        try {
            ResponseEntity respuesta = restTemplate.getForEntity(ujapack, Void.class);
        } catch (HttpClientErrorException e) {
            return false;
        }

        return true;

    }

    /**
     * Metodo para guardar una persona a traves de /ujapack/personas/
     *
     * @param persona que se desea guardar
     * @return si se ha podido gusrdar
     */
    public boolean guardarPersona(DTOPersona persona) {

        try {

            ResponseEntity<DTOPersona> respuestaPersona = restTemplate.postForEntity(
                    ujapack + "/personas",
                    persona,
                    DTOPersona.class
            );

        } catch (HttpClientErrorException e) {
            
            if(HttpStatus.NOT_FOUND == e.getStatusCode()){
            
                JFrameNotificacion i = new JFrameNotificacion(TipoNotificacion.localidadNoValida);
                i.setVisible(true);
                i.setLocationRelativeTo(null);
            }

            if(HttpStatus.NOT_ACCEPTABLE == e.getStatusCode()){
            
                JFrameNotificacion i = new JFrameNotificacion(TipoNotificacion.campoInvalido);
                i.setVisible(true);
                i.setLocationRelativeTo(null);
            }
            
            if(HttpStatus.BAD_REQUEST == e.getStatusCode()){
            
                JFrameNotificacion i = new JFrameNotificacion(TipoNotificacion.campoInvalido);
                i.setVisible(true);
                i.setLocationRelativeTo(null);
            }
            
            if(HttpStatus.CONFLICT == e.getStatusCode()){
            
                JFrameNotificacion i = new JFrameNotificacion(TipoNotificacion.personaYaExistente);
                i.setVisible(true);
                i.setLocationRelativeTo(null);
            }
            return false;
        }
        return true;

    }

    /**
     * Metodo para crear un envio
     *
     * @param envio DTO del envio que se quiere crear
     * @return el localizador del envio si se crea
     */
    public int crearEnvio(DTOEnvio envio) {

        try {

            ResponseEntity<DTOEnvio> respuestaEnvio = restTemplate.postForEntity(
                    ujapack + "/envios/",
                    envio,
                    DTOEnvio.class
            );

            int localizador = respuestaEnvio.getBody().getLocalizador();
            return localizador;

        } catch (HttpClientErrorException e) {
            return 0;
        }

    }

    /**
     * Metodo para ver los datos del envio
     *
     * @param localizador del envio
     * @return DTO del envio con los datos apropiados
     */
    public DTOEnvio verDatosEnvio(int localizador) {

        try {
            ResponseEntity<DTOEnvio> respuestaEstado = restTemplate.getForEntity(
                    ujapack + "/envios/" + localizador,
                    DTOEnvio.class);

            return respuestaEstado.getBody();

        } catch (HttpClientErrorException e) {

            return new DTOEnvio();
        }

    }
    
    
    /**
     * Metodo para ver el estado del envio
     *
     * @param localizador del envio
     * @return DTO del envio con los datos apropiados
     */
    public DTOEnvio verEstado(int localizador) {

        try {
            ResponseEntity<DTOEnvio> respuestaEstado = restTemplate.getForEntity(
                    ujapack + "/envios/" + localizador + "/estado",
                    DTOEnvio.class);

            return respuestaEstado.getBody();

        } catch (HttpClientErrorException e) {

            return new DTOEnvio();
        }

    }

    /**
     * Metodo para ver la lista de las etapas del envio
     *
     * @param localizador del envio
     * @return lista de DTO de las etapas
     */
    public DTOEtapa[] verListado(int localizador) {

        try {
            ResponseEntity<DTOEtapa[]> respuestaEtapas = restTemplate.getForEntity(
                    ujapack + "/envios/{localizador}/etapas",
                    DTOEtapa[].class,
                    localizador);

            return respuestaEtapas.getBody();

        } catch (HttpClientErrorException e) {

            if(HttpStatus.NOT_FOUND == e.getStatusCode()){
            
                JFrameNotificacion i = new JFrameNotificacion(TipoNotificacion.localizadorNoExistente);
                i.setVisible(true);
                i.setLocationRelativeTo(null);
            }

           
            return null;
        }

    }

    /**
     * Metodo para realizar la llegada o la salida de un envio
     *
     * @param localizador del envio
     * @param idPunto desde el que se solicita la peticion
     * @param seleccion cual accion se quiere realizar
     * @return si se ha realizado la accion
     */
    public boolean moverEnvio(int localizador, int idPunto, String seleccion) {

        try {
            String Punto = ujapack + "/envios/" + localizador + "/etapas/" + idPunto;
            URI URIPunto = URI.create(Punto);

            switch (seleccion) {

                case "Llegada":

                    DTOAccionEnvio accionLlegada = new DTOAccionEnvio(
                            Accion.llegada);

                    restTemplate.put(URIPunto, accionLlegada);
                    break;

                case "Salida":

                    DTOAccionEnvio accionSalida = new DTOAccionEnvio(
                            Accion.salida);
                    restTemplate.put(URIPunto, accionSalida);
                    break;
            }

            return true;

        } catch (HttpClientErrorException e) {

            if(HttpStatus.NOT_FOUND == e.getStatusCode()){
            
                JFrameNotificacion i = new JFrameNotificacion(TipoNotificacion.localizadorNoExistente);
                i.setVisible(true);
                i.setLocationRelativeTo(null);
            }

            if(HttpStatus.CONFLICT == e.getStatusCode()){
            
                JFrameNotificacion i = new JFrameNotificacion(TipoNotificacion.envioExtraviado);
                i.setVisible(true);
                i.setLocationRelativeTo(null);
            }
            
            if(HttpStatus.NOT_ACCEPTABLE == e.getStatusCode()){
            
                JFrameNotificacion i = new JFrameNotificacion(TipoNotificacion.accionIncorrecta);
                i.setVisible(true);
                i.setLocationRelativeTo(null);
            }
            
            return false;
        }

    }

    /**
     * Metodo para ver los paquetes extraviados
     *
     * @return de la lista de los envios
     */
    public DTOEnvio[] verExtraviados() {

        try {

            DTOEnvio[] respuestaVerExtraviados = restTemplate.getForEntity(
                    ujapack + "/envios/extraviados",
                    DTOEnvio[].class).getBody();

            return respuestaVerExtraviados;

        } catch (HttpClientErrorException e) {

            return null;
        }

    }

    /**
     * Metodo para ver el porcentaje de extraviados con una restriccion
     *
     * @param rango que restriccion se quiere aplicar
     * @return el valor del porcentaje en float
     */
    public float porcentajeExtraviados(String rango) {

        try {

            float respuestaPorcentajeExtraviados = restTemplate.getForObject(
                    ujapack + "/envios/extraviados/porcentaje/" + rango,
                    float.class);

            return respuestaPorcentajeExtraviados;

        } catch (HttpClientErrorException e) {

            return -1;
        }

    }

    /**
     * Metodo para entregar un paquete
     *
     * @param localizador del envio
     * @return si se ha realizado la entrega
     */
    public boolean entregarEnvio(int localizador) {

        try {

            String entrega = ujapack + "/envios/" + localizador + "/etapas/destino";
            URI URIentrega = URI.create(entrega);

            DTOAccionEnvio accionEntrega = new DTOAccionEnvio(
                    LocalDateTime.now()
            );

            restTemplate.put(URIentrega, accionEntrega);

            return true;

        } catch (HttpClientErrorException e) {

            if(HttpStatus.NOT_FOUND == e.getStatusCode()){
            
                JFrameNotificacion i = new JFrameNotificacion(TipoNotificacion.localizadorNoExistente);
                i.setVisible(true);
                i.setLocationRelativeTo(null);
            }

            if(HttpStatus.CONFLICT == e.getStatusCode()){
            
                JFrameNotificacion i = new JFrameNotificacion(TipoNotificacion.envioExtraviado);
                i.setVisible(true);
                i.setLocationRelativeTo(null);
            }
            
            if(HttpStatus.NOT_ACCEPTABLE == e.getStatusCode()){
            
                JFrameNotificacion i = new JFrameNotificacion(TipoNotificacion.accionIncorrecta);
                i.setVisible(true);
                i.setLocationRelativeTo(null);
            }
            return false;
        }

    }

    /**
     * @return the usuario
     */
    public String getUsuario() {
        return usuario;
    }

}
