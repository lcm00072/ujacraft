/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.REST.DTO;


/**
 *
 * @author 1hlui
 */
public class DTOPersona {

    String dni;
    
    /* Nombre de la persona */
    String nombre;
    
    /* Apellidos de la persona */
    String apellidos;
    
    /* Localidad donde reside */
    String localidad;
    
    /* Direccion de la persona */
    String direccion;

    public DTOPersona(String dni, String nombre, String apellidos, String localidad, String direccion) {
        this.dni = dni;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.localidad = localidad;
        this.direccion = direccion;
    }
    
    public DTOPersona() {
    }
    

    public String getDni() {
        return dni;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getLocalidad() {
        return localidad;
    }

    public String getDireccion() {
        return direccion;
    }
    
//    public Persona aPersona() {
//        return new Persona(dni, nombre, apellidos, localidad, direccion);
//    }

    
}
