/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujapack.entidades;


import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 *
 * @author 1hlui
 */
public class PersonaTest {
    

    
    public PersonaTest() {
    }

    @Test
    public void testValidacionPersona() {
        
        Persona persona = new Persona(
                "77329978E", 
                "Luis", 
                "Correa",
                "Alicante",
                "Calle escaleras");
       
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<Persona>> violations = validator.validate(persona);
                
        Assertions.assertThat(violations).isEmpty();
    }
        
    @Test
    public void testComprobacionDNImal() {
        
        //Dni que no sigue la expresion regular
        
        Persona persona = new Persona(
                "77329E", 
                "Luis", 
                "Correa",
                "Alicante",
                "Calle escaleras");
        
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<Persona>> violations = validator.validate(persona);
                
        Assertions.assertThat(violations).isNotEmpty();
    }

    
}
