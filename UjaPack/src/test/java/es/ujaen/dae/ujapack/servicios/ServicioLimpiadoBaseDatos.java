/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujapack.servicios;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * Servicio auxiliar de borrado de los datos en la base de datos (sólo para testing)
 * @author 1hluis
 */

@Service
public class ServicioLimpiadoBaseDatos {
    @PersistenceContext
    EntityManager em;
    
    @Autowired
    TransactionTemplate transactionTemplate;

    /** 
     * Lista de entidades a borrar. Ojo: el orden es muy importante
     * para evitar errores de violación de integridad 
     */
    final String[] entidades = {
        "Etapa", 
        //"Oficina",
        //"Centro", 
        //"PuntoControl", 
        //"Provincia",
        "envio_ruta",
        "Envio", 
        "Persona"            
    };
    
    final String[] entidades2 = {
        "Etapa", 
        //"Oficina",
        //"Centro", 
        //"PuntoControl", 
        //"Provincia",
        //"Envio_ruta",
        "Envio", 
        "Persona"            
    };
    
    
    final String deleteFrom = "delete from ";
    
    /** Realizar borrado */
    public void limpiar() {
        transactionTemplate.executeWithoutResult(transactionStatus -> {
            for (String tabla : entidades) {
                em.createQuery(deleteFrom + tabla).executeUpdate();
            }
        });
    }
    void limpiarConEtapas() {
        transactionTemplate.executeWithoutResult(transactionStatus -> {
            for (String tabla : entidades2) {
                em.createQuery(deleteFrom + tabla).executeUpdate();
            }
        });
    }
}
