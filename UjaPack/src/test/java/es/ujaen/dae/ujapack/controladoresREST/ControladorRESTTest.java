/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujapack.controladoresREST;

import es.ujaen.dae.ujapack.controladoresREST.DTO.DTOAccionEnvio;
import es.ujaen.dae.ujapack.controladoresREST.DTO.DTOAccionEnvio.Accion;
import es.ujaen.dae.ujapack.controladoresREST.DTO.DTOEnvio;
import es.ujaen.dae.ujapack.controladoresREST.DTO.DTOEnvioExtraviado;
import es.ujaen.dae.ujapack.controladoresREST.DTO.DTOEtapa;
import es.ujaen.dae.ujapack.controladoresREST.DTO.DTOPersona;
import es.ujaen.dae.ujapack.entidades.Envio;
import es.ujaen.dae.ujapack.entidades.Envio.Estado;
import es.ujaen.dae.ujapack.servicios.ServicioLimpiadoBaseDatos;
import es.ujaen.dae.ujapack.utilidades.Rango;
import es.ujaen.dae.ujapack.utilidades.Rango.RangoFecha;
import java.net.URI;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import javax.annotation.PostConstruct;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import static org.springframework.web.servlet.function.RequestPredicates.headers;

/**
 *
 * @author 1hlui
 */
@SpringBootTest(classes = es.ujaen.dae.ujapack.app.UjaPackApp.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ControladorRESTTest {

    @LocalServerPort
    int localPort;

    @Autowired
    MappingJackson2HttpMessageConverter springBootJacksonConverter;

    RestTemplateBuilder restTemplateBuilder;

    @PostConstruct
    void crearRestTemplateBuilder() {
        restTemplateBuilder = new RestTemplateBuilder()
                .rootUri("http://localhost:" + localPort + "/ujapack")
                .additionalMessageConverters(springBootJacksonConverter);
    }

    /**
     * Guardamos una persona correctamente
     */
    @Test
    public void testGuardarPersona() {

        DTOPersona persona = new DTOPersona(
                "20269980D",
                "Diego",
                "Goku",
                "Jaen",
                "su casa");

        TestRestTemplate restTemplate = new TestRestTemplate(restTemplateBuilder.basicAuthentication("ujapack", "secret"));
        ResponseEntity<DTOPersona> respuesta = restTemplate.postForEntity(
                "/personas",
                persona,
                DTOPersona.class
        );

        Assertions.assertThat(respuesta.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }

    /**
     * Creamos un envio bien hecho y esperamos que se entreque con exito Se
     * comprueba el ver estado Se comprueba ver el listado de etapas del envio
     */
    @Test
    public void pasoCompletoRutaEnvioEntregado() {

        /* TestRestTemplate de cada tipo de role administrados por la seguridad */
 /* Role Administrativo */
        TestRestTemplate restTemplateAdministrativo = new TestRestTemplate(restTemplateBuilder.basicAuthentication("ujapack", "secret"));

        /* Role Repartidor */
        TestRestTemplate restTemplateRepartidor = new TestRestTemplate(restTemplateBuilder.basicAuthentication("repartidor", "paquete"));

        /* Role Cliente */
        TestRestTemplate restTemplateCliente = new TestRestTemplate(restTemplateBuilder.basicAuthentication("cliente", "usuario"));

        DTOPersona persona = new DTOPersona(
                "12345678E",
                "Vicente",
                "Vives",
                "Jaen",
                "el barrio");

        ResponseEntity<DTOPersona> respuestaPersona = restTemplateAdministrativo.postForEntity(
                "/personas",
                persona,
                DTOPersona.class
        );

        Assertions.assertThat(respuestaPersona.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        DTOPersona persona2 = new DTOPersona(
                "77379980E",
                "Luis",
                "Isco",
                "Alicante",
                "mi casa");

        ResponseEntity<DTOPersona> respuestaPersona2 = restTemplateAdministrativo.postForEntity(
                "/personas",
                persona2,
                DTOPersona.class
        );

        Assertions.assertThat(respuestaPersona2.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        DTOEnvio envio = new DTOEnvio(
                10,
                1200,
                "12345678E",
                "77379980E");

        /* Se crea el envio */
        ResponseEntity<DTOEnvio> respuestaEnvio = restTemplateAdministrativo.postForEntity(
                "/envios",
                envio,
                DTOEnvio.class
        );

        Assertions.assertThat(respuestaEnvio.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        String localizador = Integer.toString(respuestaEnvio.getBody().getLocalizador());

        /* Declaramos las acciones Llegada y Salida */
        DTOAccionEnvio accionLlegada = new DTOAccionEnvio(
                Accion.llegada);

        DTOAccionEnvio accionSalida = new DTOAccionEnvio(
                Accion.salida);

        /* Declaramos la accion de entrega con la fecha de esta */
        DTOAccionEnvio accionEntrega = new DTOAccionEnvio(
                LocalDateTime.parse("2021-01-01T10:00:03")
        );

        /* Creamos la url del punto a utilizar para notificar la llegada o salida */
        String Punto16 = "/envios/" + localizador + "/etapas/16";
        URI URIPunto16 = URI.create(Punto16);

        restTemplateAdministrativo.put(URIPunto16, accionLlegada);
        restTemplateAdministrativo.put(URIPunto16, accionSalida);

        String Punto1 = "/envios/" + localizador + "/etapas/1";
        URI URIPunto1 = URI.create(Punto1);

        restTemplateAdministrativo.put(URIPunto1, accionLlegada);
        restTemplateAdministrativo.put(URIPunto1, accionSalida);

        String Punto3 = "/envios/" + localizador + "/etapas/3";
        URI URIPunto3 = URI.create(Punto3);

        restTemplateAdministrativo.put(URIPunto3, accionLlegada);
        restTemplateAdministrativo.put(URIPunto3, accionSalida);

        String Punto30 = "/envios/" + localizador + "/etapas/30";
        URI URIPunto30 = URI.create(Punto30);

        restTemplateAdministrativo.put(URIPunto30, accionLlegada);
        restTemplateAdministrativo.put(URIPunto30, accionSalida);

        /* url de entrega para el envio */
        String entrega = "/envios/" + localizador + "/etapas/destino";
        URI URIentrega = URI.create(entrega);

        restTemplateRepartidor.put(URIentrega, accionEntrega);

        /* Respuesta con el estado del envio, debe ser Entregado */
        ResponseEntity<DTOEnvio> respuestaEstado = restTemplateCliente.getForEntity(
                "/envios/" + localizador + "/estado",
                DTOEnvio.class);

        Assertions.assertThat(respuestaEstado.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(respuestaEstado.getBody().getEstado()).isEqualTo(Estado.Entregado);

        /* Respuesta con la lista de las etapas del envio, debe tener 5 etapas contando la de entrega */
        DTOEtapa[] respuestaEtapas = restTemplateCliente.getForEntity(
                "/envios/{localizador}/etapas",
                DTOEtapa[].class,
                localizador).getBody();

        Assertions.assertThat(respuestaEtapas).hasSize(5);
        Assertions.assertThat(respuestaEtapas[4].getFechaEntrega()).isNotNull();
    }

    /**
     * Test para comprobar que se devuelven envios extraviados ademas del
     * porcentaje de envios extraviados
     */
    @Test
    public void enviosExtraviados() {

        DTOPersona persona1 = new DTOPersona(
                "22222222D",
                "Juande",
                "Manco",
                "Alava",
                "la costa");

        DTOPersona persona2 = new DTOPersona(
                "44444444D",
                "Pedro",
                "Fifa",
                "Madrid",
                "el agua");

        TestRestTemplate restTemplate = new TestRestTemplate(restTemplateBuilder.basicAuthentication("ujapack", "secret"));

        ResponseEntity<DTOPersona> respuestaPersona1 = restTemplate.postForEntity(
                "/personas",
                persona1,
                DTOPersona.class
        );

        Assertions.assertThat(respuestaPersona1.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        ResponseEntity<DTOPersona> respuestaPersona2 = restTemplate.postForEntity(
                "/personas",
                persona2,
                DTOPersona.class
        );

        Assertions.assertThat(respuestaPersona2.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        DTOEnvio envio = new DTOEnvio(
                8,
                1500,
                "22222222D",
                "44444444D");

        ResponseEntity<DTOEnvio> respuestaEnvio = restTemplate.postForEntity(
                "/envios",
                envio,
                DTOEnvio.class
        );

        Assertions.assertThat(respuestaEnvio.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        DTOEnvio envio2 = new DTOEnvio(
                12,
                1900,
                "22222222D",
                "44444444D");

        ResponseEntity<DTOEnvio> respuestaEnvio2 = restTemplate.postForEntity(
                "/envios",
                envio2,
                DTOEnvio.class
        );

        DTOEnvioExtraviado envioExtraviado = new DTOEnvioExtraviado(
                respuestaEnvio2.getBody().getLocalizador(),
                LocalDateTime.parse("2021-01-01T16:00:03"),
                null
        );

        ResponseEntity<DTOEnvioExtraviado> respuestaGuardarExtraviado = restTemplate.postForEntity(
                "/envios/extraviados",
                envioExtraviado,
                DTOEnvioExtraviado.class
        );

        Assertions.assertThat(respuestaGuardarExtraviado.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        DTOEnvio[] respuestaVerExtraviados = restTemplate.getForEntity(
                "/envios/extraviados",
                DTOEnvio[].class).getBody();

        Assertions.assertThat(respuestaVerExtraviados).hasSize(1);
        Assertions.assertThat(respuestaVerExtraviados[0].getLocalizador()).isEqualTo(respuestaEnvio2.getBody().getLocalizador());
        Assertions.assertThat(respuestaVerExtraviados[0].getLocalizador()).isEqualTo(respuestaEnvio2.getBody().getLocalizador());

        float respuestaPorcentajeExtraviados = restTemplate.getForObject(
                "/envios/extraviados/porcentaje/year",
                float.class);

        Assertions.assertThat(respuestaPorcentajeExtraviados).isEqualTo(50.0f);
    }

    /**
     *
     */
    @Test
    public void crearEnvioMalTest() {

        /* Role Administrativo */
        TestRestTemplate restTemplateAdministrativo = new TestRestTemplate(restTemplateBuilder.basicAuthentication("ujapack", "secret"));

        /* Role Repartidor */
        TestRestTemplate restTemplateRepartidor = new TestRestTemplate(restTemplateBuilder.basicAuthentication("repartidor", "paquete"));

        /* Role Cliente */
        TestRestTemplate restTemplateCliente = new TestRestTemplate(restTemplateBuilder.basicAuthentication("cliente", "usuario"));

        DTOPersona persona = new DTOPersona(
                "55555555B",
                "Lucas",
                "Pato",
                "Murcia",
                "la iglesia");

        ResponseEntity<DTOPersona> respuestaPersona = restTemplateAdministrativo.postForEntity(
                "/personas",
                persona,
                DTOPersona.class
        );

        Assertions.assertThat(respuestaPersona.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        DTOPersona persona2 = new DTOPersona(
                "66666666A",
                "Rodrigo",
                "Moreno",
                "Madrid",
                "la feria");

        ResponseEntity<DTOPersona> respuestaPersona2 = restTemplateAdministrativo.postForEntity(
                "/personas",
                persona2,
                DTOPersona.class
        );

        Assertions.assertThat(respuestaPersona2.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        /* Ponemos el DNI del remitente mal por lo que como la persona no esta registrada, no se puede crear el envio */
        DTOEnvio envio = new DTOEnvio(
                80,
                1550,
                "55555555C",
                "66666666A");

        /* Se crea el envio */
        ResponseEntity<DTOEnvio> respuestaEnvio = restTemplateAdministrativo.postForEntity(
                "/envios",
                envio,
                DTOEnvio.class
        );

        Assertions.assertThat(respuestaEnvio.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

        /* Ahora ponemos el envio correctamente */
        DTOEnvio envio2 = new DTOEnvio(
                80,
                1550,
                "55555555B",
                "66666666A");

        /* Se crea el envio */
        ResponseEntity<DTOEnvio> respuestaEnvio2 = restTemplateAdministrativo.postForEntity(
                "/envios",
                envio2,
                DTOEnvio.class
        );

        Assertions.assertThat(respuestaEnvio2.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        /* Declaramos las acciones Llegada y Salida */
        DTOAccionEnvio accionLlegada = new DTOAccionEnvio(
                Accion.llegada);

        DTOAccionEnvio accionSalida = new DTOAccionEnvio(
                Accion.salida);

        /* Declaramos la accion de entrega con la fecha de esta */
        DTOAccionEnvio accionEntrega = new DTOAccionEnvio(
                LocalDateTime.parse("2021-01-02T10:00:03")
        );

        String localizador = Integer.toString(respuestaEnvio2.getBody().getLocalizador());

        /* Creamos la url del punto a utilizar para notificar la llegada o salida */
        String Punto31 = "/envios/" + localizador + "/etapas/31";
        URI URIPunto31 = URI.create(Punto31);

        restTemplateAdministrativo.put(URIPunto31, accionLlegada);
        restTemplateAdministrativo.put(URIPunto31, accionSalida);

        String Punto3 = "/envios/" + localizador + "/etapas/3";
        URI URIPunto3 = URI.create(Punto3);

        restTemplateAdministrativo.put(URIPunto3, accionLlegada);
        restTemplateAdministrativo.put(URIPunto3, accionSalida);

        String Punto2 = "/envios/" + localizador + "/etapas/2";
        URI URIPunto2 = URI.create(Punto2);

        restTemplateAdministrativo.put(URIPunto2, accionLlegada);

        /* No ponemos la salida del punto 2 de control por ello los demás PUT no se realizarán */
        //restTemplateAdministrativo.put(URIPunto2, accionSalida);
        
        String Punto9 = "/envios/" + localizador + "/etapas/9";
        URI URIPunto9 = URI.create(Punto9);

        restTemplateAdministrativo.put(URIPunto9, accionLlegada);
        restTemplateAdministrativo.put(URIPunto9, accionSalida);

        String Punto60 = "/envios/" + localizador + "/etapas/60";
        URI URIPunto60 = URI.create(Punto60);

        restTemplateAdministrativo.put(URIPunto60, accionLlegada);
        restTemplateAdministrativo.put(URIPunto60, accionSalida);

        /* url de entrega para el envio que no podrá realizarse */
        String entrega = "/envios/" + localizador + "/etapas/destino";
        URI URIentrega = URI.create(entrega);

        restTemplateRepartidor.put(URIentrega, accionEntrega);

        /* Respuesta con el estado del envio, debe ser En transito */
        ResponseEntity<DTOEnvio> respuestaEstado = restTemplateCliente.getForEntity(
                "/envios/" + localizador + "/estado",
                DTOEnvio.class);

        Assertions.assertThat(respuestaEstado.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(respuestaEstado.getBody().getEstado()).isEqualTo(Estado.EnTransito);

        /* Respuesta con la lista de las etapas del envio, debe tener 3 etapas 
        *  porque no crea ninguna después del punto de control 2 
        */
        DTOEtapa[] respuestaEtapas = restTemplateCliente.getForEntity(
                "/envios/{localizador}/etapas",
                DTOEtapa[].class,
                localizador).getBody();

        Assertions.assertThat(respuestaEtapas).hasSize(3);
        Assertions.assertThat(respuestaEtapas[2].getPuntoPasado()).isEqualTo(2);
    }
}
