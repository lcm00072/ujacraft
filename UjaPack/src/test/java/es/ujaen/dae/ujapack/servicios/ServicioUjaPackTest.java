/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujapack.servicios;

import es.ujaen.dae.ujapack.entidades.Envio;
import es.ujaen.dae.ujapack.entidades.Envio.Estado;
import es.ujaen.dae.ujapack.entidades.Persona;
import es.ujaen.dae.ujapack.excepciones.EnvioEnTransito;
import es.ujaen.dae.ujapack.utilidades.Rango;
import es.ujaen.dae.ujapack.utilidades.Rango.RangoFecha;
import java.time.LocalDateTime;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 *
 * @author 1hlui
 */
@SpringBootTest(classes = es.ujaen.dae.ujapack.app.UjaPackApp.class)
public class ServicioUjaPackTest {

    @Autowired
    ServicioUjaPack servicioUjaPack;

//    @Autowired
//    ServicioLimpiadoBaseDatos limpiadorBaseDatos;
    //@BeforeEach
//    void limpiarBaseDatos() {
//        limpiadorBaseDatos.limpiar();
//    }

    
    @Test
    public void testAccesoServicioUjaPack() {
        Assertions.assertThat(servicioUjaPack).isNotNull();
    }

    /**
     * Creamos un envio bien hecho y esperamos su exito *
     */
    @Test
    public void testValidacionPersona() {

        Persona persona = new Persona(
                "77329970E",
                "Luis",
                "Correa",
                "Alicante",
                "Calle escaleras");

        servicioUjaPack.repositorioPersonas.guardar(persona);

        String dni = "77329971E";

        Assertions.assertThat(servicioUjaPack.repositorioPersonas.buscar(dni).isPresent());

    }

    /**
     * Creamos un envio bien hecho y esperamos su exito *
     */
    @Test
    public void crearEnvio() {

        Persona Persona1 = new Persona("77379981E", "Doctor", "Malo", "Murcia", "El resi");
        Persona Persona2 = new Persona("77379982E", "Diego", "Barrales", "Lugo", "Su casa");

        Persona Persona3 = new Persona("17379981E", "Pepe", "Correa", "Leon", "El resi");
        Persona Persona4 = new Persona("27379982E", "Mario", "Diaz", "Lugo", "Su casa");

        servicioUjaPack.repositorioPersonas.guardar(Persona1);
        servicioUjaPack.repositorioPersonas.guardar(Persona2);

        servicioUjaPack.repositorioPersonas.guardar(Persona3);
        servicioUjaPack.repositorioPersonas.guardar(Persona4);

        Envio envio1 = servicioUjaPack.crearEnvio(7, 1600, Persona1, Persona2);
        Envio envio2 = servicioUjaPack.crearEnvio(10, 1900, Persona3, Persona4);

        Assertions.assertThat(envio1.getLocalizador()).isNotNull();
        Assertions.assertThat(envio2.getLocalizador()).isNotNull();

    }

    /**
     * Calculamos el importe de un envio y esperamos que sea mayor que 0 *
     */
    @Test
    public void envioImporte() {

        Persona Persona1 = new Persona("27379974E", "Carlos", "Cano", "Valencia", "El resi");
        Persona Persona2 = new Persona("17379935E", "Javier", "Lopez", "Jaen", "Su casa");

        servicioUjaPack.repositorioPersonas.guardar(Persona1);
        servicioUjaPack.repositorioPersonas.guardar(Persona2);

        Envio envio = servicioUjaPack.crearEnvio(7, 1600, Persona1, Persona2);

        Assertions.assertThat(envio.getImporte()).isGreaterThan(0);
    }

    /**
     * Enviamos un envio y se espera que se entregue bien
     *
     * @throws java.lang.InterruptedException *
     */
    @Test
    public void entregarEnvio() throws InterruptedException {

        Persona Persona1 = new Persona("11111111E", "Mono", "Yeguas", "Madrid", "El resi");
        Persona Persona2 = new Persona("22222222E", "Laura", "Lendinez", "Valencia", "Su casa");

        servicioUjaPack.repositorioPersonas.guardar(Persona1);
        servicioUjaPack.repositorioPersonas.guardar(Persona2);

        Envio envio = servicioUjaPack.crearEnvio(7, 1600, Persona1, Persona2);

        
        int localizador = envio.getLocalizador();

        servicioUjaPack.llegadaEnvio(localizador, 60);
        servicioUjaPack.salidaEnvio(localizador, 60);

        servicioUjaPack.llegadaEnvio(localizador, 9);
        servicioUjaPack.salidaEnvio(localizador, 9);

        servicioUjaPack.llegadaEnvio(localizador, 6);
        servicioUjaPack.salidaEnvio(localizador, 6);

        servicioUjaPack.llegadaEnvio(localizador, 2);
        servicioUjaPack.salidaEnvio(localizador, 2);

        servicioUjaPack.llegadaEnvio(localizador, 3);
        servicioUjaPack.salidaEnvio(localizador, 3);
        Assertions.assertThat(servicioUjaPack.verEstado(localizador)).isEqualTo(Estado.EnTransito);

        servicioUjaPack.llegadaEnvio(localizador, 29);
        servicioUjaPack.salidaEnvio(localizador, 29);
        Assertions.assertThat(servicioUjaPack.verEstado(localizador)).isEqualTo(Estado.EnReparto);

        LocalDateTime fechaEntrega = LocalDateTime.now();

        servicioUjaPack.entregarEnvio(localizador, fechaEntrega);

        Assertions.assertThat(servicioUjaPack.verEstado(localizador)).isEqualTo(Estado.Entregado);

        Assertions.assertThat(servicioUjaPack.repositorioEnvios.buscar(localizador).isPresent());

    }

    @Test
    public void envioExtraviado() throws InterruptedException {

        Persona Persona1 = new Persona("33333333B", "Rodolfo", "Gimenez", "Las-Palmas", "Mansion");
        Persona Persona2 = new Persona("44444444C", "Patricia", "Martinez", "Vizcaya", "UJA");

        Persona Persona3 = new Persona("55555555D", "Pablo", "Fernandez", "Orense", "Valderrrana");
        Persona Persona4 = new Persona("66666666E", "Alvarillo", "Info", "Salamanca", "El chalet");

        servicioUjaPack.repositorioPersonas.guardar(Persona1);
        servicioUjaPack.repositorioPersonas.guardar(Persona2);

        servicioUjaPack.repositorioPersonas.guardar(Persona3);
        servicioUjaPack.repositorioPersonas.guardar(Persona4);

        Envio envio1 = servicioUjaPack.crearEnvio(7, 1600, Persona1, Persona2);

        int localizador = envio1.getLocalizador();

        Envio envio2 = servicioUjaPack.crearEnvio(9, 1400, Persona3, Persona4);

        int localizador2 = envio2.getLocalizador();

        servicioUjaPack.llegadaEnvio(localizador, 62);
        servicioUjaPack.salidaEnvio9dias(localizador, 62);

        servicioUjaPack.llegadaEnvio(localizador2, 57);
        servicioUjaPack.salidaEnvio9dias(localizador2, 57);

        Thread.sleep(15 * 1000);

        Assertions.assertThat(servicioUjaPack.verEstado(localizador)).isEqualTo(Estado.Extraviado);

        List<Envio> envios = servicioUjaPack.verExtraviados();

        int pos = servicioUjaPack.buscarLocalizadorEnLista(envios, localizador);
        int pos2 = servicioUjaPack.buscarLocalizadorEnLista(envios, localizador2);

        Rango tipoRango = new Rango(RangoFecha.month);

        Assertions.assertThat(envios.get(pos).getLocalizador()).isEqualTo(localizador);
        Assertions.assertThat(envios.get(pos2).getLocalizador()).isEqualTo(localizador2);
        Assertions.assertThat(servicioUjaPack.porcentajeExtraviados(tipoRango)).isGreaterThan(0);

    }

    /**
     * Da error porque intentamos entregar el paquete antes de que salga del
     * ultimo punto de control *
     */
    @Test
    public void entregarEnvioMal() throws InterruptedException {

        Persona Persona1 = new Persona("12379987X", "Javi", "Martinez", "Jaen", "Fuente");
        Persona Persona2 = new Persona("77323981N", "Pablo", "Roca", "Barcelona", "El baño");

        servicioUjaPack.repositorioPersonas.guardar(Persona1);
        servicioUjaPack.repositorioPersonas.guardar(Persona2);

        Envio envio = servicioUjaPack.crearEnvio(7, 1600, Persona1, Persona2);

        int localizador = envio.getLocalizador();
        
        servicioUjaPack.llegadaEnvio(localizador, 16);
        servicioUjaPack.salidaEnvio(localizador, 16);

        LocalDateTime fechaEntrega = LocalDateTime.now();
        
        Assertions.assertThatThrownBy(() -> {
            servicioUjaPack.entregarEnvio(localizador, fechaEntrega);
        })
                .isInstanceOf(EnvioEnTransito.class);

    }

    /**
     * Hacemos un envio a la misma oficina esperando que se haga bien *
     */
    @Test
    public void envioMismaOficina() throws InterruptedException {

        Persona Persona1 = new Persona("12379989X", "Javi", "Martinez", "Jaen", "Fuente");
        Persona Persona2 = new Persona("77344983E", "Pablo", "Roca", "Jaen", "El baño");

        servicioUjaPack.repositorioPersonas.guardar(Persona1);
        servicioUjaPack.repositorioPersonas.guardar(Persona2);

        Envio envio = servicioUjaPack.crearEnvio(7, 1600, Persona1, Persona2);

        int localizador = envio.getLocalizador();
        
        servicioUjaPack.llegadaEnvio(localizador, 16);
        servicioUjaPack.salidaEnvio(localizador, 16);

        LocalDateTime fechaEntrega = LocalDateTime.now();
        
        servicioUjaPack.entregarEnvio(localizador, fechaEntrega);

        Assertions.assertThat(servicioUjaPack.verEstado(localizador)).isEqualTo(Estado.Entregado);

    }

    /**
     * Enviamos al mismo centro pero hacemos mal la entrega *
     */
    @Test
    public void envioMismoCentroMal() throws InterruptedException {

        //Ponemos el mismo Centro
        Persona Persona1 = new Persona("12379089X", "Javi", "Rodri", "Alicante", "Fuente");
        Persona Persona2 = new Persona("72554980B", "Juan", "Feo", "Valencia", "El baño");

        servicioUjaPack.repositorioPersonas.guardar(Persona1);
        servicioUjaPack.repositorioPersonas.guardar(Persona2);

        Envio envio = servicioUjaPack.crearEnvio(7, 1600, Persona1, Persona2);

        int localizador = envio.getLocalizador();
        
        servicioUjaPack.llegadaEnvio(localizador, 30);
        servicioUjaPack.salidaEnvio(localizador, 30);

        servicioUjaPack.llegadaEnvio(localizador, 3);
        servicioUjaPack.salidaEnvio(localizador, 3);

        servicioUjaPack.llegadaEnvio(localizador, 29);

        /**
         * No creamos la salida del envio del ultimo punto por lo que no se
         * puede entregar *
         */
        //servicioUjaPack.salidaEnvio(localizador, 29
        
        LocalDateTime fechaEntrega = LocalDateTime.now();
        
        Assertions.assertThatThrownBy(() -> {
            servicioUjaPack.entregarEnvio(localizador, fechaEntrega);
        })
                .isInstanceOf(EnvioEnTransito.class);

    }

    /**
     * Comprobamos que el estado del pedido
     *
     * @throws java.lang.InterruptedException *
     */
    @Test
    public void comprobarEstadoEnReparto() throws InterruptedException {

        Persona Persona1 = new Persona("42369980D", "Cristiano", "Ronaldo", "Jaen", "Fuente");
        Persona Persona2 = new Persona("71334921R", "Lionel", "Messi", "Jaen", "El baño");

        servicioUjaPack.repositorioPersonas.guardar(Persona1);
        servicioUjaPack.repositorioPersonas.guardar(Persona2);

        Envio envio = servicioUjaPack.crearEnvio(7, 1600, Persona1, Persona2);

        int localizador = envio.getLocalizador();
        
        servicioUjaPack.llegadaEnvio(localizador, 16);
        servicioUjaPack.salidaEnvio(localizador, 16);

        Assertions.assertThat(servicioUjaPack.verEstado(localizador).equals(Estado.EnReparto));

    }
}
