/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujapack.entidades;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 *
 * @author Diego
 */
@Entity
public class EnvioExtraviado implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;
    
    @OneToOne
    private Envio envioExtraviado;
    
    private LocalDateTime FechaLlegada;
    
    private LocalDateTime FechaSalida;
    
    
    public EnvioExtraviado() {
        
    }

    public EnvioExtraviado(Envio envioExtraviado, LocalDateTime FechaLlegada, LocalDateTime FechaSalida) {
        //this.id = id;
        this.envioExtraviado = envioExtraviado;
        this.FechaLlegada = FechaLlegada;
        this.FechaSalida = FechaSalida;
    }

    
    /**
     * @return the envioExtraviado
     */
    public Envio getEnvioExtraviado() {
        return envioExtraviado;
    }

    /**
     * @param envioExtraviado the envioExtraviado to set
     */
    public void setEnvioExtraviado(Envio envioExtraviado) {
        this.envioExtraviado = envioExtraviado;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the FechaLlegada
     */
    public LocalDateTime getFechaLlegada() {
        return FechaLlegada;
    }

    /**
     * @param FechaLlegada the FechaLlegada to set
     */
    public void setFechaLlegada(LocalDateTime FechaLlegada) {
        this.FechaLlegada = FechaLlegada;
    }

    /**
     * @return the FechaSalida
     */
    public LocalDateTime getFechaSalida() {
        return FechaSalida;
    }

    /**
     * @param FechaSalida the FechaSalida to set
     */
    public void setFechaSalida(LocalDateTime FechaSalida) {
        this.FechaSalida = FechaSalida;
    }

}