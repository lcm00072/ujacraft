/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujapack.controladoresREST.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import es.ujaen.dae.ujapack.entidades.Envio;
import es.ujaen.dae.ujapack.entidades.EnvioExtraviado;
import java.time.LocalDateTime;

/**
 *
 * @author 1hlui
 */
public class DTOEnvioExtraviado {
    
    public int id;
    
    public int envioExtraviado;
    
    @JsonFormat
    public LocalDateTime FechaLlegada;
    
    @JsonFormat
    public LocalDateTime FechaSalida;

    public DTOEnvioExtraviado(int envioExtraviado, LocalDateTime FechaLlegada, LocalDateTime FechaSalida) {
        this.envioExtraviado = envioExtraviado;
        this.FechaLlegada = FechaLlegada;
        this.FechaSalida = FechaSalida;
    }

    public DTOEnvioExtraviado(EnvioExtraviado envioExtraviado) {
        this.envioExtraviado = envioExtraviado.getEnvioExtraviado().getLocalizador();
        this.FechaLlegada = envioExtraviado.getFechaLlegada();
        this.FechaSalida = envioExtraviado.getFechaSalida();
    }
    
    public DTOEnvioExtraviado() {
    }
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the envioExtraviado
     */
    public int getEnvioExtraviado() {
        return envioExtraviado;
    }

    /**
     * @return the FechaLlegada
     */
    public LocalDateTime getFechaLlegada() {
        return FechaLlegada;
    }

    /**
     * @return the FechaSalida
     */
    public LocalDateTime getFechaSalida() {
        return FechaSalida;
    }
    
}
