/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujapack.repositorios;

import es.ujaen.dae.ujapack.entidades.Envio;
import es.ujaen.dae.ujapack.entidades.EnvioExtraviado;
import es.ujaen.dae.ujapack.entidades.Etapa;
import es.ujaen.dae.ujapack.entidades.PuntoControl;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author 1hlui
 */
@Repository
@Transactional
public class RepositorioEnvios {

    @PersistenceContext
    EntityManager em;

    public Optional<Envio> buscar(int localizador) {
        return Optional.ofNullable(em.find(Envio.class, localizador));
    }

    public void guardar(Envio envio) {
        em.persist(envio);
    }

    public void actualizarEnvio(Envio envio) {
        em.merge(envio);
    }

    public void actualizarEtapa(Etapa etapa) {
        em.merge(etapa);
    }

    public void nuevaEtapa(Envio envio, Etapa etapa) {

        em.merge(etapa);
        envio = em.merge(envio);

    }


    public void guardarEtapa(Etapa etapa) {
        em.persist(etapa);
    }

    
    public Optional<Etapa> buscarEtapa(LocalDateTime FechaLlegada) {
        return Optional.ofNullable(em.find(Etapa.class, FechaLlegada));
    }


    public Envio devolverEnvio(int localizador) {

        Envio envio = em.find(Envio.class, localizador);
        envio.getRuta().size();
        envio.getEtapas().size();
        return envio;
    }

    public Etapa devolverEtapa(LocalDateTime fecha) {

        Etapa etapa = em.find(Etapa.class, fecha);

        return etapa;
    }

    public List<Integer> obtenerLocalizadores() {
        String selectFrom = "SELECT localizador FROM Envio";
        Query query = em.createQuery(selectFrom);

        List<Integer> envios = query.getResultList();

        return envios;
    }

    public int cantidadEnvios() {
        String selectFrom = "SELECT COUNT(localizador) FROM Envio";
        Query query = em.createQuery(selectFrom);

        int total = Integer.parseInt(query.getSingleResult().toString());

        return total;
    }
    
    public Object ultimaEtapaId() {
        String selectFrom = "SELECT MAX(idEtapa) AS mayorId FROM Etapa";
        Query query = em.createQuery(selectFrom);

        Object total = query.getSingleResult();

        return total;
    }

    public List<Envio> obtenerEnviosExtraviados() {
        String selectFrom = "SELECT a.envioExtraviado FROM EnvioExtraviado a";
        Query query = em.createQuery(selectFrom);

        List<Envio> envios = query.getResultList();

        return envios;
    }
    
    public List<Envio> envioExtraviadoFechaLlegada() {
        
        String selectFrom = "SELECT a.envioExtraviado FROM EnvioExtraviado a where (DATE_FORMAT(a.FechaLlegada, '%Y-%m-%d') = DATE_FORMAT(curdate(), '%Y-%m-%d')) AND a.FechaSalida IS NULL";
        Query query = em.createQuery(selectFrom);

        List<Envio> envios = query.getResultList();

        return envios;
    }
    
    public List<Envio> envioExtraviadoFechaSalida() {
        
        String selectFrom = "SELECT a.envioExtraviado FROM EnvioExtraviado a where DATE_FORMAT(a.FechaSalida, '%Y-%m-%d') = DATE_FORMAT(curdate(), '%Y-%m-%d')";
        Query query = em.createQuery(selectFrom);

        List<Envio> envios = query.getResultList();

        return envios;
    }
    
    public List<Envio> envioExtraviadoFechaLlegadaMes() {
        
        String selectFrom = "SELECT a.envioExtraviado FROM EnvioExtraviado a where (DATE_FORMAT(a.FechaLlegada, '%Y-%m') = DATE_FORMAT(curdate(), '%Y-%m')) AND a.FechaSalida IS NULL";
        Query query = em.createQuery(selectFrom);

        List<Envio> envios = query.getResultList();

        return envios;
    }
    
    public List<Envio> envioExtraviadoFechaSalidaMes() {
        
        String selectFrom = "SELECT a.envioExtraviado FROM EnvioExtraviado a where DATE_FORMAT(a.FechaSalida, '%Y-%m') = DATE_FORMAT(curdate(), '%Y-%m')";
        Query query = em.createQuery(selectFrom);

        List<Envio> envios = query.getResultList();

        return envios;
    }
    
    public List<Envio> envioExtraviadoFechaLlegadaYear() {
        
        String selectFrom = "SELECT a.envioExtraviado FROM EnvioExtraviado a where (DATE_FORMAT(a.FechaLlegada, '%Y') = DATE_FORMAT(curdate(), '%Y')) AND a.FechaSalida IS NULL";
        Query query = em.createQuery(selectFrom);

        List<Envio> envios = query.getResultList();

        return envios;
    }
    
    public List<Envio> envioExtraviadoFechaSalidaYear() {
        
        String selectFrom = "SELECT a.envioExtraviado FROM EnvioExtraviado a where DATE_FORMAT(a.FechaSalida, '%Y') = DATE_FORMAT(curdate(), '%Y')";
        Query query = em.createQuery(selectFrom);

        List<Envio> envios = query.getResultList();

        return envios;
    }
    

    public void guardarExtraviado(EnvioExtraviado envioExtraviado) {
        
        em.merge(envioExtraviado.getEnvioExtraviado());
        em.persist(envioExtraviado);
    }
    
}
