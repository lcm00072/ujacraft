/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujapack.controladoresREST.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import es.ujaen.dae.ujapack.entidades.Etapa;
import java.time.LocalDateTime;

/**
 *
 * @author 1hlui
 */
public class DTOEtapa {

    int idEtapa;

    /**
     * Fecha de llegada al punto de control
     */
    
    @JsonFormat
    LocalDateTime FechaLlegada;

    /**
     * Fecha de salida al punto de control
     */
    @JsonFormat
    LocalDateTime FechaSalida;

    @JsonFormat
    LocalDateTime FechaEntrega;

    private String direccionEntrega;

    public DTOEtapa(int idEtapa, LocalDateTime FechaLlegada, LocalDateTime FechaSalida, int puntoPasado) {
        this.idEtapa = idEtapa;
        this.FechaLlegada = FechaLlegada;
        this.FechaSalida = FechaSalida;
        this.puntoPasado = puntoPasado;
    }

    public DTOEtapa(Etapa etapa) {
        this.idEtapa = etapa.getIdEtapa();
        this.FechaLlegada = etapa.getFechaLlegada();
        this.FechaSalida = etapa.getFechaSalida();
        this.puntoPasado = etapa.getPuntoPasado().getId();
        this.FechaEntrega = etapa.getFechaEntrega();
    }

    public DTOEtapa(LocalDateTime FechaEntrega, String direccionEntrega) {

        this.FechaEntrega = FechaEntrega;
        this.direccionEntrega = direccionEntrega;
    }

    public DTOEtapa() {
    }
    int puntoPasado;

    public int getIdEtapa() {
        return idEtapa;
    }

    public LocalDateTime getFechaLlegada() {
        return FechaLlegada;
    }

    public LocalDateTime getFechaSalida() {
        return FechaSalida;
    }

    public int getPuntoPasado() {
        return puntoPasado;
    }

    public LocalDateTime getFechaEntrega() {
        return FechaEntrega;
    }

    /**
     * @return the direccionEntrega
     */
    public String getDireccionEntrega() {
        return direccionEntrega;
    }

}
