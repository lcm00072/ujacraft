/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujapack.controladoresREST.DTO;

import es.ujaen.dae.ujapack.entidades.Persona;
import es.ujaen.dae.ujapack.utilidades.ExprReg;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 *
 * @author 1hlui
 */
public class DTOPersona {

    @Size(min=9, max=9)
    @Pattern(regexp=ExprReg.DNI)
    String dni;
    
    /* Nombre de la persona */
    String nombre;
    
    /* Apellidos de la persona */
    String apellidos;
    
    /* Localidad donde reside */
    String localidad;
    
    /* Direccion de la persona */
    String direccion;

    public DTOPersona(String dni, String nombre, String apellidos, String localidad, String direccion) {
        this.dni = dni;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.localidad = localidad;
        this.direccion = direccion;
    }
    
    public DTOPersona(Persona persona) {
        this.dni = persona.getDni();
        this.nombre = persona.getNombre();
        this.apellidos = persona.getApellidos();
        this.localidad = persona.getLocalidad();
        this.direccion = persona.getDireccion();
    }

    public String getDni() {
        return dni;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getLocalidad() {
        return localidad;
    }

    public String getDireccion() {
        return direccion;
    }
    
    public Persona aPersona() {
        return new Persona(dni, nombre, apellidos, localidad, direccion);
    }

    
}
