/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujapack.repositorios;

import es.ujaen.dae.ujapack.entidades.Persona;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author 1hlui
 */
@Repository
@Transactional
public class RepositorioPersonas {
    @PersistenceContext
    EntityManager em;
    
    public Optional<Persona> buscar(String dni){
       return Optional.ofNullable(em.find(Persona.class, dni));
    }
    
    public void guardar(Persona persona){
        em.persist(persona);
    }
    
    public void borrarTabla(){
         final String deleteFrom = "delete from persona";
         em.createQuery(deleteFrom).executeUpdate(); 
    }
    
    public Persona devolverPersona(String dni){
        return em.find(Persona.class, dni);
    }
}
