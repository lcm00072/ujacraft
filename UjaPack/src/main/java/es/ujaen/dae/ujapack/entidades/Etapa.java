/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujapack.entidades;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author 1hlui
 */
@Entity
@Access(AccessType.FIELD)
public class Etapa implements Serializable {

    static final String STRING_SI = "S";
    static final String STRING_NO = "N";
    
    @Id
    private int idEtapa;
    /** Fecha de llegada al punto de control */ 
    LocalDateTime FechaLlegada;
    
    /** Fecha de salida al punto de control */
    LocalDateTime FechaSalida;
    
    /** Fecha de entrega*/
    LocalDateTime FechaEntrega;
    
    /** Vemos si el envio esta actualmente en esta etapa */
    @Transient
    private boolean etapaActivada;
    
    /** Punto de control donde se realiza la etapa */
    @ManyToOne
    private PuntoControl puntoPasado;
    
    
    public Etapa() {
    }

    public Etapa(LocalDateTime FechaLlegada, LocalDateTime FechaSalida, PuntoControl puntoPasado) {
        this.FechaLlegada = FechaLlegada;
        this.FechaSalida = FechaSalida;
        this.puntoPasado = puntoPasado;
        this.etapaActivada = false;
    }
    
    public Etapa(LocalDateTime FechaLlegada, LocalDateTime FechaSalida, LocalDateTime FechaEntrega,PuntoControl puntoPasado) {
        this.FechaLlegada = FechaLlegada;
        this.FechaSalida = FechaSalida;
        this.FechaEntrega = FechaEntrega;
        this.puntoPasado = puntoPasado;
        this.etapaActivada = false;
    }

    /**
     * @return the FechaLlegada
     */
    public LocalDateTime getFechaLlegada() {
        return FechaLlegada;
    }

    /**
     * @param FechaLlegada the FechaLlegada to set
     */
    public void setFechaLlegada(LocalDateTime FechaLlegada) {
        this.FechaLlegada = FechaLlegada;
    }

    /**
     * @return the FechaSalida
     */
    public LocalDateTime getFechaSalida() {
        return FechaSalida;
    }

    /**
     * @param FechaSalida the FechaSalida to set
     */
    public void setFechaSalida(LocalDateTime FechaSalida) {
        this.FechaSalida = FechaSalida;
    }

    /**
     * @return the puntoPasado
     */
    public PuntoControl getPuntoPasado() {
        return puntoPasado;
    }

    /**
     * @param puntoPasado the puntoPasado to set
     */
    public void setPuntoPasado(PuntoControl puntoPasado) {
        this.puntoPasado = puntoPasado;
    }

    /**
     * @return the etapaActivada
     */
    public boolean isEtapaActivada() {
        return etapaActivada;
    }

    /**
     * @param etapaActivada the etapaActivada to set
     */
    public void setEtapaActivada(boolean etapaActivada) {
        this.etapaActivada = etapaActivada;
    }

    /**
     * @return the id_etapa
     */
    public int getIdEtapa() {
        return idEtapa;
    }

    /**
     * @param idEtapa the id_etapa to set
     */
    public void setIdEtapa(int idEtapa) {
        this.idEtapa = idEtapa;
    }

    /**
     * @return the FechaEntrega
     */
    public LocalDateTime getFechaEntrega() {
        return FechaEntrega;
    }

    /**
     * @param FechaEntrega the FechaEntrega to set
     */
    public void setFechaEntrega(LocalDateTime FechaEntrega) {
        this.FechaEntrega = FechaEntrega;
    }
    
}
