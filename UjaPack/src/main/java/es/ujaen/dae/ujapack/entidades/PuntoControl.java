/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujapack.entidades;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

/**
 *
 * @author 1hlui
 */
@Entity
@Inheritance( strategy = InheritanceType.JOINED)
public class PuntoControl implements Serializable {

    /** Id del punto de control, los centros se asignaran primero y despues las oficinas */
    @Id
    protected int id;
    
    public PuntoControl() {
        super( );
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
    
}
