/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujapack.seguridad;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.stereotype.Service;

/**
 *
 * @author 1hlui
 */
@Service
@EnableWebSecurity
public class ServicioSeguridadUjaPack extends WebSecurityConfigurerAdapter {

    /* Creamos 3 tipos de usuarios en memoria 
       -Administrativo- Se encarga de crear envios, guardar personas, 
        notificar la llegada y salida de un envio y poder ver los
        datos de este
    
       -Repartidor- Se encarga de llevar el envio al destino
    
       -Cliente- Es un rol que se da a cualquier usuario sin logearse
        para poder indicar a que acciones tiene permiso acceder
    
    */
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("ujapack").roles("ADMINISTRATIVO").password("{noop}secret");
        auth.inMemoryAuthentication()
                .withUser("repartidor").roles("REPARTIDOR").password("{noop}paquete");
        auth.inMemoryAuthentication()
                .withUser("cliente").roles("CLIENTE").password("{noop}usuario");
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.csrf().disable();

        httpSecurity.httpBasic();

        /* Aqui comprobamos quienes son los usuarios que tienen acceso a nuestro servidor */
        httpSecurity.authorizeRequests().antMatchers(HttpMethod.GET,"/ujapack/")
                .hasAnyRole("CLIENTE","ADMINISTRATIVO","REPARTIDOR");
        
        httpSecurity.authorizeRequests().antMatchers(HttpMethod.PUT, "/ujapack/envios/{localizador}/etapas/destino").hasRole("REPARTIDOR");
        
        httpSecurity.authorizeRequests().antMatchers("/ujapack/personas")
                .hasRole("ADMINISTRATIVO");
        
        httpSecurity.authorizeRequests().antMatchers(HttpMethod.PUT, "/ujapack/envios/{localizador}/etapas/{idPunto}").hasRole("ADMINISTRATIVO");
        
        httpSecurity.authorizeRequests().antMatchers(HttpMethod.GET,"/ujapack/envios/{localizador}/estado")
                .hasAnyRole("CLIENTE","ADMINISTRATIVO");
        
        httpSecurity.authorizeRequests().antMatchers(HttpMethod.GET,"/ujapack/envios/{localizador}/etapas")
                .hasAnyRole("CLIENTE","ADMINISTRATIVO");

        httpSecurity.authorizeRequests().antMatchers(HttpMethod.POST, "/ujapack/envios").hasRole("ADMINISTRATIVO");
        
        
    }

}
