/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujapack.servicios;

import es.ujaen.dae.ujapack.entidades.Centro;
import es.ujaen.dae.ujapack.entidades.Oficina;
import es.ujaen.dae.ujapack.entidades.Provincia;
import es.ujaen.dae.ujapack.entidades.PuntoControl;
import es.ujaen.dae.ujapack.repositorios.RepositorioPuntoControl;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import javax.annotation.PostConstruct;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author luis
 */

@Service
public class ServicioCargaRedLogistica {
    
//    private Map<Integer, Centro> centrosMapa;
//    private Map<Integer, PuntoControl> puntosMapa;
//    private Map<Integer, Oficina> oficinasMapa;
//    
    @Autowired
    RepositorioPuntoControl repositorioPuntoControl;
    
    public ServicioCargaRedLogistica() throws IOException, FileNotFoundException, ParseException{
//        centrosMapa = new TreeMap<>();
//        puntosMapa = new TreeMap<>();
//        oficinasMapa = new TreeMap<>();
    }
    
    @PostConstruct
    private void cargarJson() throws FileNotFoundException, IOException, ParseException {

        /**
         * Creamos el Json *
         */
        
        if(repositorioPuntoControl.cantidadPuntos()>0){
            return;
        }
        
        JSONParser parser = new JSONParser();

        /**
         * Creamos el objeto Json a partir del archivo *
         */
        Object obj = parser.parse(new FileReader("redujapack.json"));
        JSONObject jsonObject = (JSONObject) obj;

        /**
         * Listas donde almacenaremos Centros, Puntos de Control y Oficinas *
         */
        List<Centro> centros = new ArrayList<>();
        List<PuntoControl> puntosControl = new ArrayList<>();
        List<Oficina> oficinas = new ArrayList<>();

        /**
         * Contador para ir cambiando de Centro *
         */
        int cont = 0;

        /**
         * Bucle para almacenar los puntos de control *
         */
        for (int i = 1; i <= jsonObject.size(); i++) { //Tomamos el tamaño

            String busCentros = String.valueOf(i); //Creamos la variable para buscar el centro por el numero
            JSONObject jsonCentros = (JSONObject) jsonObject.get(busCentros);

            String nombreC = jsonCentros.get("nombre").toString(); //Guardamos el nombre del centro en una variable
            String sedeC = jsonCentros.get("localizacion").toString(); //Guardamos la sede del centro en una variable

            PuntoControl nuevoPunto = new PuntoControl(); //Creamos un nuevo Punto de Control asociado al centro
            int idPunto = puntosControl.size() + 1; //Se utiliza para saber el id que tenemos que asociar al Punto
            nuevoPunto.setId(idPunto); //Lo asignamos
            puntosControl.add(nuevoPunto); //Se añade a la Lista de puntos de Control

            Centro nuevoCentro = new Centro(nombreC, i, sedeC, (int) idPunto); //Nuevo Centro con los atributos
            centros.add(nuevoCentro); //Añade el centro a la lista de Centros

            JSONArray nConexiones = (JSONArray) jsonCentros.get("conexiones"); //Cargamos las conexiones del centro
            Iterator iteratorCon = nConexiones.iterator(); //Creamos un iterador para pasar por ellas
            List<Centro> conexiones = new ArrayList<>(); //Lista de las conexiones que va a tener el centro

            /**
             * Bucle para guardar las conexiones en la Lista *
             */
//            while (iteratorCon.hasNext()) {
//
//                String numCon = iteratorCon.next().toString(); //Se guarda en String la conexion
//                conexiones.add(numCon); //La añadimos a la lista de conexiones del centro
//
//            }
//
//            centros.get(i - 1).setConexiones(conexiones); //Asociamos la lista de conexiones a su Centro

        }
        
        /**
         * Bucle para crear las Provincias *
         */
        for (int i = 1; i <= jsonObject.size(); i++) {

            List<Provincia> provincias = new ArrayList<>(); //Lista de provincias auxiliar

            String busqueda = String.valueOf(i); //String para buscar
            JSONObject jsonObject2 = (JSONObject) jsonObject.get(busqueda); //Objeto con el centro 
            JSONArray arrayP = (JSONArray) jsonObject2.get("provincias"); //Array con las provincias del Json

            /**
             * Capturamos las excepciones *
             */

            try {
                Iterator iterator = arrayP.iterator(); //Iterador del array  de Provincias

                while (iterator.hasNext()) { //Bucle para pasar por el Array de Provincias

                    String nombrePro = iterator.next().toString(); //Guardamos el nombre de la Provincia
                    Provincia nuevaProvincia = new Provincia(nombrePro); //Creamos una nueva Provincia
                    //repositorioPuntoControl.guardarProvincia(nuevaProvincia);
                    provincias.add(nuevaProvincia); //Añadimos la provincia a la lista auxiliar

                    Oficina nuevaOficina = new Oficina(nuevaProvincia); //Creamos una nueva Oficina asociada a la Provincia

                    int idPunto = puntosControl.size() + 1; //Calculamos el id de la Oficina
                    nuevaOficina.setId(idPunto); //Asignamos el id
                    oficinas.add(nuevaOficina); //Añadimos la Oficina

                    PuntoControl puntoOfi = new PuntoControl(); //Nuevo Punto de Control asociado a la Oficina
                    puntoOfi.setId(idPunto); //Le asociamos el id al Punto de Control de la Oficina
                    puntosControl.add(puntoOfi); //Añadimos el punto de Control de la oficina a la Lista de Puntos

                }

                /**
                 * Guardamos la lista de provincias en cada centro *
                 */
                centros.get(cont).setProvincias(provincias);
                cont++; //Aumentamos el contador para pasar de Centro

                /**
                 * Capturamos las excepciones *
                 */
} catch (Exception ex) {
                //System.err.println("Error: " + ex.toString());
            } finally {

            }

        }
        
                
        
        
        
        int TAMcentros = centros.size();
        int TAMoficinas = oficinas.size();
        int TAMpuntos = puntosControl.size();
        int contMap = 0;

        /**
         * Añadimos los centros de la lista al mapa de centros *
         */
        while (contMap < TAMcentros) {
  
            //getCentrosMapa().put(centros.get(contMap).getId(), centros.get(contMap));
            
        repositorioPuntoControl.guardarCentro(centros.get(contMap));
            contMap++;
        }
        contMap = 0;

        /**
         * Añadimos las oficinas de la lista al mapa de oficinas *
         */
        while (contMap < TAMoficinas) {
            //getOficinasMapa().put(oficinas.get(contMap).getId(), oficinas.get(contMap));
        repositorioPuntoControl.guardarOficina(oficinas.get(contMap));
            contMap++;
        }
        contMap = 0;

         for (int i = 1; i <= jsonObject.size(); i++) {

                 String busCentros = String.valueOf(i); //Creamos la variable para buscar el centro por el numero
            JSONObject jsonCentros = (JSONObject) jsonObject.get(busCentros);


            JSONArray nConexiones = (JSONArray) jsonCentros.get("conexiones"); //Cargamos las conexiones del centro
            ListIterator listIterator = nConexiones.listIterator(); //Creamos un iterador para pasar por ellas
            List<Centro> conexiones = new ArrayList<>(); //Lista de las conexiones que va a tener el centro
            
            for (Object nConex : nConexiones){
                String numCon = nConex.toString();

                int numCentro = Integer.parseInt(numCon);
                //repositorioPuntoControl.devolverCentro(i).nuevaConexion(repositorioPuntoControl.devolverCentro(numCentro));
                Centro centroOfi = repositorioPuntoControl.devolverCentro(i);
                Centro centroCon = repositorioPuntoControl.devolverCentro(numCentro);
                repositorioPuntoControl.nuevaConexion(centroOfi, centroCon);
            }
            
            /**
             * Bucle para guardar las conexiones en la Lista *
             */
//            while (listIterator.hasNext()) {
//                System.out.println(listIterator.next());
//                String numCon = listIterator.next().toString(); //Se guarda en String la conexion
//                int numCentro = Integer.parseInt(numCon);
//                repositorioPuntoControl.devolverCentro(i).nuevaConexion(repositorioPuntoControl.devolverCentro(numCentro));
//
//            }

            //centros.get(i - 1).setConexiones(conexiones); //Asociamos la lista de conexiones a su Centro
                 
                 }
    }

//    /**
//     * @return the centrosMapa
//     */
//    public Map<Integer, Centro> getCentrosMapa() {
//        return centrosMapa;
//    }
//
//    /**
//     * @param centrosMapa the centrosMapa to set
//     */
//    public void setCentrosMapa(Map<Integer, Centro> centrosMapa) {
//        this.centrosMapa = centrosMapa;
//    }
//
//    /**
//     * @return the puntosMapa
//     */
//    public Map<Integer, PuntoControl> getPuntosMapa() {
//        return puntosMapa;
//    }
//
//    /**
//     * @param puntosMapa the puntosMapa to set
//     */
//    public void setPuntosMapa(Map<Integer, PuntoControl> puntosMapa) {
//        this.puntosMapa = puntosMapa;
//    }
//
//    /**
//     * @return the oficinasMapa
//     */
//    public Map<Integer, Oficina> getOficinasMapa() {
//        return oficinasMapa;
//    }
//
//    /**
//     * @param oficinasMapa the oficinasMapa to set
//     */
//    public void setOficinasMapa(Map<Integer, Oficina> oficinasMapa) {
//        this.oficinasMapa = oficinasMapa;
//    }
    
}
