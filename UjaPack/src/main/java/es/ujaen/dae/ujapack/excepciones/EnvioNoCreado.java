/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujapack.excepciones;

/**
 * Excepción provocada al intentar acceder a un envio no creado
 * @author 1hlui
 */

public class EnvioNoCreado extends RuntimeException {

    public EnvioNoCreado() {
    }
}