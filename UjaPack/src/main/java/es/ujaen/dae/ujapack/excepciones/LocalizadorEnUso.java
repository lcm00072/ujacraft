/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujapack.excepciones;

/**
 * Excepción provocada cuando se intenta asignar un localizador
 * a un paquete cuando ya está en uso ese localizador
 * @author 1hlui
 */
public class LocalizadorEnUso extends RuntimeException {

    public LocalizadorEnUso() {
    }
}
