/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujapack.servicios;

import es.ujaen.dae.ujapack.entidades.Centro;
import es.ujaen.dae.ujapack.entidades.Envio;
import es.ujaen.dae.ujapack.entidades.Envio.Estado;
import es.ujaen.dae.ujapack.entidades.EnvioExtraviado;
import es.ujaen.dae.ujapack.entidades.Etapa;
import es.ujaen.dae.ujapack.entidades.Oficina;
import es.ujaen.dae.ujapack.entidades.Persona;
import es.ujaen.dae.ujapack.entidades.Provincia;
import es.ujaen.dae.ujapack.entidades.PuntoControl;
import es.ujaen.dae.ujapack.excepciones.AccionNoCorrespondiente;
import es.ujaen.dae.ujapack.excepciones.EnvioEnTransito;
import es.ujaen.dae.ujapack.excepciones.EnvioExtraviadoExcepcion;
import es.ujaen.dae.ujapack.excepciones.EnvioNoCreado;
import es.ujaen.dae.ujapack.excepciones.EnvioSinEtapas;
import es.ujaen.dae.ujapack.excepciones.PersonaNoEncontrada;
import es.ujaen.dae.ujapack.excepciones.PersonaYaGuardada;
import es.ujaen.dae.ujapack.excepciones.PuntoNoCorrespondiente;
import es.ujaen.dae.ujapack.repositorios.RepositorioEnvios;
import es.ujaen.dae.ujapack.repositorios.RepositorioPersonas;
import es.ujaen.dae.ujapack.repositorios.RepositorioPuntoControl;
import es.ujaen.dae.ujapack.utilidades.Rango;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDateTime;
import static java.time.temporal.ChronoUnit.DAYS;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import org.apache.commons.math3.util.Precision;
import org.hibernate.Hibernate;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Clase con los servicios del sistema de envios de UjaPack
 *
 * @author 1hlui
 */
@Service
@EnableScheduling
public class ServicioUjaPack {

    /**
     * Mapa con los envios que realiza el servicio
     */
//    private Map<String, Envio> envios;
//
//    private Map<Integer, Centro> centrosMapa;
//    private Map<Integer, PuntoControl> puntosMapa;
//    private Map<Integer, Oficina> oficinasMapa;
    
    @Autowired
    RepositorioPersonas repositorioPersonas;

    @Autowired
    RepositorioEnvios repositorioEnvios;

    @Autowired
    RepositorioPuntoControl repositorioPuntoControl;

    /**
     * Constructor del servicio, se agregan excepciones debido a la lectura de
     * un fichero
     *
     * @throws java.io.FileNotFoundException
     * @throws org.json.simple.parser.ParseException
     */
    public ServicioUjaPack() throws FileNotFoundException, ParseException, IOException {
        //envios = new TreeMap<>();
        //centrosMapa = new TreeMap<>();
        //puntosMapa = new TreeMap<>();
        //oficinasMapa = new TreeMap<>();

    }

    @Autowired
    ServicioCargaRedLogistica servicioCargaRedLogistica;

    
    /**
     * Crea un envio
     *
     * @param persona persona con su localidad asignada
     * @return true en caso de que la localidad de la persona esté en la base de datos
     */
    public boolean comprobarLocalidadExistente(Persona persona) {

        return repositorioPuntoControl.buscarProvincia(persona.getLocalidad()).isPresent();
    }

    /**
     * Buscar si una persona se encuentra en la base de datos
     *
     * @param dni de la persona a buscar
     * @return devuelve la persona en caso de que la encuentre
     * @throws PersonaNoEncontrada()
     */
    public Persona buscarPersona(String dni) {

        if (repositorioPersonas.buscar(dni).isPresent()) {

            return repositorioPersonas.devolverPersona(dni);
        } else {
            throw new PersonaNoEncontrada();
        }
    }

    /**
     * Guardar una persona en la base de datos
     *
     * @param personaBuscada persona que se quiere guardar
     * @return devuelve la persona cuando se guarde
     * @throws PersonaYaEncontrada()
     */
    public Persona guardarPersona(Persona personaBuscada) {

        if (!repositorioPersonas.buscar(personaBuscada.getDni()).isPresent()) {
            repositorioPersonas.guardar(personaBuscada);
            return repositorioPersonas.devolverPersona(personaBuscada.getDni());
        } else {
            throw new PersonaYaGuardada();
        }
    }

    /**
     * Buscar si un envio existe en la base de datos
     *
     * @param localizador del envio a buscar
     * @return devuelve el envio en caso de que la encuentre
     * @throws EnvioNoCreado()
     */
    public Envio buscarEnvio(int localizador) {

        if (repositorioEnvios.buscar(localizador).isPresent()) {

            return repositorioEnvios.devolverEnvio(localizador);
        } else {
            throw new EnvioNoCreado();
        }
    }

    /**
     * Guardar un envio extraviado
     *
     * @param envioExtraviado envio que se quiere guardar en extraviados
     */
    public void guardarExtraviado(EnvioExtraviado envioExtraviado) {

        repositorioEnvios.guardarExtraviado(envioExtraviado);
    }

    /**
     * Actualiza un envio
     *
     * @param envio a actualizar
     */
    public void actualizarEnvio(Envio envio) {

        repositorioEnvios.actualizarEnvio(envio);
    }

    /**
     * --------------------------------- Crea un envio
     *
     * @param npeso el peso del envio
     * @param ndim dimension del envio en cm2
     * @param nremitente persona que realiza envio
     * @param ndestinatario persona que recibe el envio
     * @return el localizador del envio
     */
    public Envio crearEnvio(int npeso, int ndim, Persona nremitente, Persona ndestinatario) {

        Envio nuevoEnvio = new Envio(npeso, ndim, nremitente, ndestinatario);

        int localizador = nuevoEnvio.getLocalizador();

        while (repositorioEnvios.buscar(localizador).isPresent()) {

            localizador = nuevoEnvio.crearLocalizador();
        }

        nuevoEnvio.setLocalizador(localizador);

        List<PuntoControl> rutaEnvio = calcularRuta(nremitente, ndestinatario);

        nuevoEnvio.setRuta(rutaEnvio);

        float nuevoImporte = calcularImporte(npeso, ndim, rutaEnvio);

        nuevoEnvio.setImporte(nuevoImporte);

        repositorioEnvios.guardar(nuevoEnvio);

        return nuevoEnvio;
    }

    /**
     * Comprobacion de si el punto esta en la ruta
     *
     * @param rutaEnvio ruta del envio
     * @param puntoControlado el punto que queremos comprobar
     * @return devuelve true si esta en la ruta o false si no
     */
    private Boolean comprobarPuntoEnRuta(List<PuntoControl> rutaEnvio, PuntoControl puntoControlado) {

        return rutaEnvio.stream().anyMatch(punto -> (punto.getId() == puntoControlado.getId()));
    }


    /**
     * Metodo para comprobar si en el punto de salida o entrada existe el punto
     * en la ruta y si es asi comprobar que sea la etapa correcta
     *
     * @param nlocalizador localizador del envio
     * @param nId Id del punto de control donde se registra la llega
     * @param tipoDeAccion Para saber si es llegada del envio(true) o
     * salida(false)
     */
    private PuntoControl siguienteEtapa(int nlocalizador, int nId, Boolean tipoDeAccion) {

        Envio envio = repositorioEnvios.devolverEnvio(nlocalizador);

        /* Si el envio no está en transito no se puede recibir o salir de un punto */
        if(envio.getEstado()!=Estado.EnTransito){
            throw new AccionNoCorrespondiente();
        }
        
        List<PuntoControl> rutaEnvio = envio.getRuta();
        
        /* Con esta variable comprobaremos el punto desde el que se hace la solicitud */
        PuntoControl puntoControlado = repositorioPuntoControl.devolverPunto(nId);

        boolean comprobacion = comprobarPuntoEnRuta(rutaEnvio, puntoControlado);

        if (comprobacion == false) {
            throw new PuntoNoCorrespondiente();
        }

        int tamEtapas = envio.getEtapas().size();
        

        /* Se comprueba si aún not tiene ninguna etapa */
        if (tamEtapas == 0) {

            /* Si el tipo de accion es llegada y el punto desde el que se solicita es el primer punto de la ruta lo devolvemos */
            if (tipoDeAccion == true && rutaEnvio.get(0).getId() == (puntoControlado.getId())) {

                return puntoControlado;
            } else {
                
                /* En caso de que se solicite la salida o no sea el punto se lanza una excepcion */
                throw new AccionNoCorrespondiente();
            }

        /* Caso en el que ya haya por lo menos una etapa */
        } else {
            
            /* Almacenamos la ultima etapa del envio */
            Etapa compararEtapa = envio.ultimaEtapa();

            /* Si la accion es de llegada y la ultima etapa no tiene fecha de salida es imposible realizar la llegada al nuevo punto */
            if (tipoDeAccion == true && envio.ultimaEtapa().getFechaSalida() == null) {

                throw new AccionNoCorrespondiente();

            /* Si la accion es de salida, no tiene fecha de salida la ultima etapa y el punto desde el que se solicita es el correcto */    
            } else if (tipoDeAccion == false && compararEtapa.getFechaSalida() == null
                    && puntoControlado.getId() == compararEtapa.getPuntoPasado().getId()) {

                return puntoControlado;
            
            /* Si la accion es de salida, no tiene fecha de salida la ultima etapa y el punto desde el que se solicita no es el correcto, lanza una excepcion */
            } else if (tipoDeAccion == false && compararEtapa.getFechaSalida() == null
                    && puntoControlado.getId() != compararEtapa.getPuntoPasado().getId()) {

                throw new AccionNoCorrespondiente();
            }

            boolean esSiguiente = false;

            /* Bucle para comprobar si el punto desde el que se solicita es el que toca*/
            for (PuntoControl punto : rutaEnvio) {

                /* Entra en el caso en de que sea el siguiente */
                if (esSiguiente == true && punto.getId() == puntoControlado.getId()) {

                    return punto;
                }

                /* Entra en el caso de que no entre en el anterior if() porque no sería el punto que corresponde */
                if (esSiguiente == true) {

                    throw new AccionNoCorrespondiente();
                }

                if (punto.getId() == compararEtapa.getPuntoPasado().getId()) {
                    esSiguiente = true;
                }

            }

        }

        return puntoControlado;

    }

    /**
     * Crea la llegada del envio al punto de
     * control asignandole una fecha de llegada
     *
     * @param nlocalizador localizador del envio
     * @param nId Id del punto de control donde se registra la llega
     */
    public void llegadaEnvio(int nlocalizador, int nId) {

        if (!repositorioEnvios.buscar(nlocalizador).isPresent()) {

            throw new EnvioNoCreado();
        }

        
        Envio envio = repositorioEnvios.devolverEnvio(nlocalizador);

        if (envio.getEstado().equals(Estado.Extraviado)) {
            throw new EnvioExtraviadoExcepcion();
        }

        boolean llegada = true;
        PuntoControl puntoSiguiente = siguienteEtapa(nlocalizador, nId, llegada);

        int idEtapa = 0;
        Object ultimaEtapaId = repositorioEnvios.ultimaEtapaId();

        if (ultimaEtapaId == null) {
            idEtapa = 1;
        } else {

            idEtapa = Integer.parseInt(ultimaEtapaId.toString()) + 1;
        }

        LocalDateTime nuevaFechaLlegada = LocalDateTime.now();
        LocalDateTime nuevaFechaSalida = null;
        Etapa etapaActual = new Etapa(nuevaFechaLlegada, nuevaFechaSalida, puntoSiguiente);
        etapaActual.setIdEtapa(idEtapa);
        etapaActual.setEtapaActivada(true);

        repositorioEnvios.nuevaEtapa(envio, etapaActual);

        envio.nuevaEtapa(etapaActual);
        repositorioEnvios.actualizarEnvio(envio);

    }

    /**
     * Crea la salida del envio al punto de
     * control asignandole una fecha de llegada
     *
     * @param nlocalizador localizador del envio
     * @param nId Id del punto de control donde se registra la llega
     * @throws java.lang.InterruptedException
     */
    public void salidaEnvio(int nlocalizador, int nId) throws InterruptedException {

        if (!repositorioEnvios.buscar(nlocalizador).isPresent()) {
            throw new EnvioNoCreado();
        }

        Envio envio = repositorioEnvios.devolverEnvio(nlocalizador);

        if (envio.getEstado().equals(Estado.Extraviado)) {
            throw new EnvioExtraviadoExcepcion();
        }

        boolean salida = false;
        PuntoControl puntoSiguiente = siguienteEtapa(nlocalizador, nId, salida);

        if (puntoSiguiente == null) {
            return;
        }

        LocalDateTime nuevaFechaSalida = LocalDateTime.now();

        List<Etapa> etapasEnvio = envio.getEtapas();
        
        for (Etapa etapa : etapasEnvio) {

            if (etapa.isEtapaActivada()) {
                etapa.setFechaSalida(nuevaFechaSalida);
                etapa.setEtapaActivada(false);

            }

        }
        

        envio.ultimaEtapa().setFechaSalida(nuevaFechaSalida);
        envio.ultimaEtapa().setEtapaActivada(false);
        repositorioEnvios.actualizarEtapa(envio.ultimaEtapa());
        repositorioEnvios.actualizarEnvio(envio);

        Boolean esUltimo = false;
        PuntoControl controlarPunto = envio.ultimaEtapa().getPuntoPasado(); //Guarda el ultimo por el que ha pasado / está
        List<PuntoControl> listaPuntos = envio.getRuta();
        int TAMlista = listaPuntos.size();

        if (listaPuntos.get(TAMlista - 1).getId() == controlarPunto.getId()) {
            esUltimo = true;

        }

        if (!envio.getEstado().equals(Estado.Extraviado)) {

            if (esUltimo == true) {

                /**
                 * Si es el ultimo punto, comprobamos si ha sido entregado *
                 */
                if (envio.getEstado().equals(Estado.Entregado)) {

                    envio.setEstado(Estado.Entregado);
                    repositorioEnvios.actualizarEnvio(envio);
                }

                /**
                 * Si es el ultimo punto y no ha sido entregado, comprobamos si
                 * tiene fecha de salida del punto para mostrarlo en reparto *
                 */
                if (envio.ultimaEtapa().getFechaSalida() == null) {
                    throw new AccionNoCorrespondiente();
                }
                if (envio.ultimaEtapa().getFechaSalida() != null) {

                    envio.setEstado(Estado.EnReparto);
                    repositorioEnvios.actualizarEnvio(envio);
                } else {

                    /**
                     * Si no tiene fecha lo mostramos en transito *
                     */
                    envio.setEstado(Estado.EnTransito);
                    repositorioEnvios.actualizarEnvio(envio);

                }
            } else {

                /**
                 * Si no es el ultimo punto de control el actual se pone en
                 * transito *
                 */
                envio.setEstado(Estado.EnTransito);
                repositorioEnvios.actualizarEnvio(envio);

            }
        }
    }

    /**
     * Crea la salida del envio al punto de
     * control asignandole una fecha de llegada
     * Se utiliza para los test y ajustar 9 dias de diferencia
     *
     * @param nlocalizador localizador del envio
     * @param nId Id del punto de control donde se registra la llega
     * @throws java.lang.InterruptedException
     */
    public void salidaEnvio9dias(int nlocalizador, int nId) throws InterruptedException {

        if (!repositorioEnvios.buscar(nlocalizador).isPresent()) {
            throw new EnvioNoCreado();
        }

        Envio envio = repositorioEnvios.devolverEnvio(nlocalizador);

        if (envio.getEstado().equals(Estado.Extraviado)) {
            throw new EnvioExtraviadoExcepcion();
        }

        boolean salida = false;
        PuntoControl puntoSiguiente = siguienteEtapa(nlocalizador, nId, salida);

        if (puntoSiguiente == null) {
            return;
        }

        LocalDateTime nuevaFechaSalida = LocalDateTime.now().minusDays(9);

        List<Etapa> etapasEnvio = envio.getEtapas();
        for (Etapa etapa : etapasEnvio) {

            if (etapa.isEtapaActivada()) {
                etapa.setFechaSalida(nuevaFechaSalida);
                etapa.setEtapaActivada(false);

            }

        }
        envio.ultimaEtapa().setFechaSalida(nuevaFechaSalida);
        envio.ultimaEtapa().setEtapaActivada(false);
        repositorioEnvios.actualizarEtapa(envio.ultimaEtapa());
        repositorioEnvios.actualizarEnvio(envio);

        Boolean esUltimo = false;
        PuntoControl controlarPunto = envio.ultimaEtapa().getPuntoPasado(); //Guarda el ultimo por el que ha pasado / está
        List<PuntoControl> listaPuntos = envio.getRuta();
        int TAMlista = listaPuntos.size();

        if (listaPuntos.get(TAMlista - 1).getId() == controlarPunto.getId()) {
            esUltimo = true;

        }

        if (!envio.getEstado().equals(Estado.Extraviado)) {

            if (esUltimo == true) {

                /**
                 * Si es el ultimo punto, comprobamos si ha sido entregado *
                 */
                if (envio.getEstado().equals(Estado.Entregado)) {

                    envio.setEstado(Estado.Entregado);
                    repositorioEnvios.actualizarEnvio(envio);
                }

                /**
                 * Si es el ultimo punto y no ha sido entregado, comprobamos si
                 * tiene fecha de salida del punto para mostrarlo en reparto *
                 */
                if (envio.ultimaEtapa().getFechaSalida() == null) {
                    throw new AccionNoCorrespondiente();
                }
                if (envio.ultimaEtapa().getFechaSalida() != null) {

                    envio.setEstado(Estado.EnReparto);
                    repositorioEnvios.actualizarEnvio(envio);
                } else {

                    /**
                     * Si no tiene fecha lo mostramos en transito *
                     */
                    envio.setEstado(Estado.EnTransito);
                    repositorioEnvios.actualizarEnvio(envio);

                }
            } else {

                /**
                 * Si no es el ultimo punto de control el actual se pone en
                 * transito *
                 */
                envio.setEstado(Estado.EnTransito);
                repositorioEnvios.actualizarEnvio(envio);

            }
        }
    }

    /**
     * Metodo para saber el estado del envio
     * Se comprueba que el envio haya sido creado si no lanza excepcion
     *
     * @param nlocalizador localizador del envio
     * @return Devuelve el estado del paquete
     */
    public Estado verEstado(int nlocalizador) { 

        if (!repositorioEnvios.buscar(nlocalizador).isPresent()) {
            throw new EnvioNoCreado();
        }

        Envio envioAux = repositorioEnvios.devolverEnvio(nlocalizador);

        return envioAux.getEstado();

    }

    /**
     * Metodo para mostrar el listado de etapas por las que ha pasado el envio 
     * Se comprueba que el envio haya sido
     * creado si no lanza excepcion Se comprueba que el envio haya pasado por
     * alguna etapa
     *
     * @param nlocalizador localizador del envio
     * @return devuelva una lista de las etapas
     */
    public List<Etapa> verListado(int nlocalizador) {

        if (!repositorioEnvios.buscar(nlocalizador).isPresent()) {
            throw new EnvioNoCreado();
        }

        Envio envioAux = repositorioEnvios.devolverEnvio(nlocalizador);

        return envioAux.getEtapas();
    }

    /**
     * Se utiliza para entregar el envio
     * cuando ha salido del ultimo punto de control Se comprueba que el envio
     * haya sido creado si no lanza excepcion Se comprueba que el envio haya
     * pasado por alguna etapa Se comprueba que el envio esté en reparto y no en
     * transito
     *
     * @param nlocalizador localizador del envio
     * @param fecha a la que se entrega el envio
     */
    public void entregarEnvio(int nlocalizador, LocalDateTime fecha) {

        if (!repositorioEnvios.buscar(nlocalizador).isPresent()) {
            throw new EnvioNoCreado();
        }

        Envio envioAux = repositorioEnvios.devolverEnvio(nlocalizador);

        if (envioAux.getEstado().equals(Estado.Extraviado)) {
            throw new EnvioExtraviadoExcepcion();
        }

        if (envioAux.getEtapas().isEmpty()) {
            throw new EnvioSinEtapas();
        }

        PuntoControl controlarPunto = envioAux.ultimaEtapa().getPuntoPasado(); //Guarda el ultimo

        List<PuntoControl> listaPuntos = envioAux.getRuta();
        int rutaTam = listaPuntos.size();
        boolean esUltimo = false;

        if (listaPuntos.get(rutaTam - 1).equals(controlarPunto)) {

            esUltimo = true;
        }

        if (esUltimo && envioAux.ultimaEtapa().getFechaSalida() != null
                && !envioAux.getEstado().equals(Estado.Entregado)) {

            envioAux.setEstado(Estado.Entregado);
            envioAux.setEntrega(fecha);
            repositorioEnvios.actualizarEnvio(envioAux);

        } else {
            throw new EnvioEnTransito();
        }

    }

    /**
     * Busca la oficina de la persona que pasemos por cabecera
     *
     * @param nPersona persona de la que se busca la localidad
     * @return se devuelve la oficina buscada
     */
    private String buscarLocalidad(Persona nPersona) {
        String oficinaBusqueda = nPersona.getLocalidad();

        return oficinaBusqueda;
    }

    /**
     * Se busca el centro de la oficina
     *
     * @param nOficina nombre de la provincia donde se encuentra la oficina
     * @return Id del centro
     */
    private int buscarCentros(String nOficina) {

        int auxiliar = 0;

        for (int i = 1; i < 11; i++) {
            List<Provincia> listaProv;
            listaProv = repositorioPuntoControl.devolverCentro(i).getProvincias();

            for (Provincia provincia : listaProv) {

                if (provincia.getN_Prov().equals(nOficina)) {
                    auxiliar = i;

                    return auxiliar;
                }
            }
        }

        return auxiliar;
    }

    /**
     * Se busca el centro de la oficina
     *
     * @param nOficina nombre de la provincia donde se encuentra la oficina
     * @return Id del centro
     */
    private int buscarOficina(String nOficina) {

        int idOf = 0;

        for (int i = 11; i < 63; i++) {
            Oficina oficina = repositorioPuntoControl.devolverOficina(i);

            if (oficina.getLocalidad().getN_Prov().equals(nOficina)) {
                idOf = oficina.getId();
                break;
            }
        }

        return idOf;
    }

    @Transactional
    private List<Centro> conseguirConexiones(int idCentro) {

        Centro centro = repositorioPuntoControl.devolverCentro(idCentro);

        Hibernate.initialize(centro);
        Hibernate.initialize(centro.getConexiones());
        List<Centro> listaCon = centro.getConexiones();

        return listaCon;
    }

    /**
     * Se encarga de buscar la lista de puntos que forman la ruta que debe
     * seguir el paquete abriendo las ramas de las conexiones Es un submetodo
     *
     * @param idOfiSalida Id de la oficina de salida
     * @param idOfiDestino Id de la oficina de llegada
     * @param idCentroSalida Id del centro de salida
     * @param idCentroLlegada Id de la centro de llegada
     * @return Lista de puntos de control
     */
    @Transactional
    private List<PuntoControl> BuscarRuta(int idOfiSalida, int idOfiDestino, int idCentroSalida, int idCentroLlegada) {

        List<PuntoControl> listaAux = new ArrayList<>();

        int idCentroDest = idCentroLlegada;

        int TAMconexiones = conseguirConexiones(idCentroSalida).size();
        boolean encontrado = false; //Para realizar bucle y abra mas ramas de las conexiones
        boolean bucle = false; //Se usa para saber que hay mas dos centros logisticos de distancias mientras las oficinas
        boolean vector = false; //Es para saber cuando se ha encontrado el camino si en las distancias medias o largas
        boolean activador = false; //Para abrir la otra rama del vector de conexiones

        Stack pilaPuntos = new Stack(); //Pila donde guardaremos la ruta
        Stack pilaAux = new Stack(); //Pila para dar la vuelta a la otra

        int aux = idCentroSalida;
        pilaPuntos.push(aux);

        List<Centro> conexionesDestino = conseguirConexiones(idCentroDest);

        while (encontrado == false) {
            for (int i = 0; i < TAMconexiones; i++) {

                i = 0;

                /**
                 * Cuando en las conexiones del centro de salida se encuentra el
                 * centro de destino *
                 */
                List<Centro> conexionesCerca = conseguirConexiones(aux);
                Centro centroDest = repositorioPuntoControl.devolverCentro(idCentroDest);
                if (conexionesCerca.contains(centroDest)
                        && bucle == false) {

                    /**
                     * Añadimos los puntos a la lista *
                     */
                    listaAux.add(repositorioPuntoControl.devolverPunto(idOfiSalida));
                    listaAux.add(repositorioPuntoControl.devolverPunto(idCentroSalida));
                    listaAux.add(repositorioPuntoControl.devolverPunto(idCentroLlegada));
                    listaAux.add(repositorioPuntoControl.devolverPunto(idOfiDestino));
                    encontrado = true;
                    return listaAux; //Devolvemos la lista

                }

                /**
                 * Si la pila de la ruta contiene el centro de destino y ya ha
                 * saltado el bucle *
                 */
                if (pilaPuntos.contains(idCentroDest)
                        && bucle == true) {

                    listaAux.add(repositorioPuntoControl.devolverPunto(idOfiSalida));

                    /**
                     * Damos la vuelta a la pila *
                     */
                    while (!pilaPuntos.empty()) {
                        pilaAux.push(pilaPuntos.pop());
                    }
                    while (!pilaAux.empty()) {
                        listaAux.add(repositorioPuntoControl.devolverPunto((int) pilaAux.pop()));

                    }
                    listaAux.add(repositorioPuntoControl.devolverPunto(idOfiDestino));
                    encontrado = true;

                    return listaAux; //Devolvemos la lista
                }
                bucle = true;

                //Medias
                List<Centro> conexiones = repositorioPuntoControl.devolverCentro(aux).getConexiones();

                int contador = 0;
                while (pilaPuntos.contains(aux) && vector == false) {

                    int TAMdestino = conexionesDestino.size();
                    for (int x = 0; x < TAMconexiones && vector == false; x++) {

                        while (contador < TAMdestino && vector == false) {

                            /**
                             * Comprueba si alguna de las conexiones del centro
                             * de destino está en la rama abierta *
                             */
                            for (Centro centro : conexiones) {

                                if (centro.getId() == conexionesDestino.get(contador).getId()
                                        && !pilaPuntos.contains(centro.getId())
                                        && !pilaPuntos.contains(conexionesDestino.get(contador).getId())) {

                                    if (conexiones.get(x).getId() != conexionesDestino.get(contador).getId()) {
                                        pilaPuntos.add(repositorioPuntoControl.devolverCentro(conexiones.get(x).getId()).getId());
                                        pilaPuntos.add(conexionesDestino.get(contador).getId());
                                    } else {
                                        pilaPuntos.add(conexionesDestino.get(contador).getId());
                                    }
                                    pilaPuntos.add(idCentroDest);
                                    vector = true;
                                }
                            }

                            contador++;
                        }
                        contador = 0;
                    }

                    //Largas
                    if (vector == false) {

                        int numExpandido = 0;
                        while (vector == false) {

                            if (activador == true) {
                                numExpandido++;
                                contador = 0;
                            }

                            /**
                             * Vector de las conexiones largas desde el centro
                             * de destino *
                             */
                            List<Centro> conexionesLargas = repositorioPuntoControl.devolverCentro(conexionesDestino.get(numExpandido).getId()).getConexiones();
                            int TAMlargas = conexionesLargas.size();

                            while (contador < TAMlargas && vector == false) {

                                if (contador < (TAMconexiones)) {

                                    /**
                                     * Vector de las conexiones medias de la
                                     * rama a partir del centro de salida *
                                     */
                                    List<Centro> conexionesMedio = repositorioPuntoControl.devolverCentro(conexiones.get(contador).getId()).getConexiones();

                                    for (int x = 0; x < TAMlargas && vector == false; x++) {

                                        int TAMmedio = conexionesMedio.size();
                                        for (int k = 0; k < TAMmedio && vector == false; k++) {

                                            /**
                                             * Comprobamos si las ramas conectan
                                             * en alguna conexion *
                                             */
                                            if (conexionesMedio.get(k).getId() == (conexionesLargas.get(x).getId())
                                                    && !pilaPuntos.contains(repositorioPuntoControl.devolverCentro(conexionesMedio.get(k).getId()).getId())
                                                    && !pilaPuntos.contains(repositorioPuntoControl.devolverCentro(conexiones.get(contador).getId()).getId())) {

                                                pilaPuntos.add(repositorioPuntoControl.devolverCentro(conexiones.get(contador).getId()).getId());
                                                pilaPuntos.add(repositorioPuntoControl.devolverCentro(conexionesMedio.get(k).getId()).getId());
                                                pilaPuntos.add(conexionesDestino.get(numExpandido).getId());
                                                pilaPuntos.add(idCentroDest);
                                                vector = true;
                                            }
                                        }
                                    }
                                    contador++;
                                } else {
                                    break;
                                }
                                activador = true;
                            }
                        }
                        break;
                    }
                }
            }
        }
        return listaAux;
    }

    /**
     * Se encarga de buscar la lista de puntos que forman la ruta que debe
     * seguir el paquete Es un submetodo
     *
     * @param nremitente Persona que realiza el envio
     * @param ndestinatario Persona que recibe el envio
     * @return Lista de puntos de control
     */
    public List<PuntoControl> calcularRuta(Persona nremitente, Persona ndestinatario) {

        String oficinaSalida = buscarLocalidad(nremitente);
        String oficinaLlegada = buscarLocalidad(ndestinatario);
        List<PuntoControl> puntosLista = new ArrayList<>();

        int idOfi = buscarOficina(oficinaSalida);
        int idOfiDest = buscarOficina(oficinaLlegada);

        /**
         * Si las oficinas son las mismas no tiene que pasar por ningun centro *
         */
        if (oficinaSalida.equals(oficinaLlegada)) {

            puntosLista.add(repositorioPuntoControl.devolverPunto(idOfi));

            return puntosLista;
        }

        int idCentroSalida = buscarCentros(oficinaSalida);
        int idCentroLlegada = buscarCentros(oficinaLlegada);

        /**
         * Si el centro de salida es el mismo que el de llegada, los asignamos
         * con sus oficinas respectivamente *
         */
        if (idCentroSalida == idCentroLlegada) {

            puntosLista.add(repositorioPuntoControl.devolverPunto(idOfi));
            puntosLista.add(repositorioPuntoControl.devolverPunto(idCentroSalida));
            puntosLista.add(repositorioPuntoControl.devolverPunto(idOfiDest));
            return puntosLista;
        }

        /**
         * En la tercera opcion si los centros no coincides calculamos la ruta *
         */
        puntosLista = BuscarRuta(idOfi, idOfiDest, idCentroSalida, idCentroLlegada);

        return puntosLista;
    }

    /**
     * Calcula el importe a partir de la formula proporcionada
     *
     * @param peso peso del envio
     * @param dim dimensiones del envio en cm2
     * @param lista Lista de puntos de control por las que pasa
     * @return el importe del envio
     */
    public float calcularImporte(int peso, int dim, List<PuntoControl> lista) {
        float importe = peso * dim * (lista.size() + 1) / 1000;
        return importe;
    }

    /**
     * Esta funcion se realiza todos los dias a las 12:00 de la noche
     * Está comentada ya que para los test usaremos la otra
     * Sabemos que no está bien implementada pero no nos ha dado tiempo a cambiarla
     * aunque funcione 
     * @throws java.lang.InterruptedException
     */
    //@Scheduled(cron = "0 0 0 * * *", zone = "Europe/Paris")
    public void envioExtraviado() throws InterruptedException {

        if (repositorioEnvios.obtenerLocalizadores().isEmpty()) {
            return;
        }
        
        List<Integer> listaLocalizadores = repositorioEnvios.obtenerLocalizadores();

        List<Envio> todosEnvios = new ArrayList<>();

        Long fechaTotal;

        listaLocalizadores.forEach(envioloc -> {
            todosEnvios.add(repositorioEnvios.devolverEnvio(envioloc));
        });

        for (Envio envio : todosEnvios) {

            if (envio.getEstado().equals(Estado.EnTransito)) {

                if (envio.getEtapas().isEmpty()) {
                    return;
                }

                Etapa ultimaEtapa = envio.ultimaEtapa();

                LocalDateTime fechaActual = LocalDateTime.now();

                LocalDateTime fechaSalidaEtapa = ultimaEtapa.getFechaSalida();
                LocalDateTime fechaLlegadaEtapa = ultimaEtapa.getFechaLlegada();

                if (ultimaEtapa.getFechaLlegada() != null) {
                    if (ultimaEtapa.getFechaSalida() == null) {

                        fechaTotal = Math.abs(DAYS.between(fechaActual, fechaLlegadaEtapa));

                        if (fechaTotal > 7) {

                            envio.setEstado(Estado.Extraviado);

                            EnvioExtraviado envioExtraviado = new EnvioExtraviado();
                            envioExtraviado.setEnvioExtraviado(envio);
                            envioExtraviado.setFechaLlegada(fechaLlegadaEtapa);

                            repositorioEnvios.guardarExtraviado(envioExtraviado);
                            repositorioEnvios.actualizarEnvio(envio);

                        }

                    } else {

                        fechaTotal = Math.abs(DAYS.between(fechaActual, fechaSalidaEtapa));

                        if (fechaTotal > 7) {

                            envio.setEstado(Estado.Extraviado);

                            EnvioExtraviado envioExtraviado = new EnvioExtraviado();
                            envioExtraviado.setEnvioExtraviado(envio);
                            envioExtraviado.setFechaLlegada(fechaLlegadaEtapa);
                            envioExtraviado.setFechaSalida(fechaSalidaEtapa);

                            repositorioEnvios.guardarExtraviado(envioExtraviado);
                            repositorioEnvios.actualizarEnvio(envio);
                        }
                    }
                }

            }
        }
    }

    /**
     * Esta funcion es la que se realiza para los tests
     * Se ejecuta cada 5 segundos para que al pausar el test de tiempo
     * a que se pongan los envios a extraviados
     * @throws java.lang.InterruptedException
     */
    @Scheduled(cron = "*/5 * * * * *")
    public void envioExtraviadoTest() throws InterruptedException {

        if (repositorioEnvios.obtenerLocalizadores().isEmpty()) {
            return;
        }
        List<Integer> listaLocalizadores = repositorioEnvios.obtenerLocalizadores();

        List<Envio> todosEnvios = new ArrayList<>();

        Long fechaTotal;

        for (Integer envioloc : listaLocalizadores) {

            todosEnvios.add(repositorioEnvios.devolverEnvio(envioloc));

        }

        for (Envio envio : todosEnvios) {

            if (envio.getEstado().equals(Estado.EnTransito)) {

                if (envio.getEtapas().isEmpty()) {
                    return;
                }

                Etapa ultimaEtapa = envio.ultimaEtapa();

                LocalDateTime fechaActual = LocalDateTime.now();

                LocalDateTime fechaSalidaEtapa = ultimaEtapa.getFechaSalida();
                LocalDateTime fechaLlegadaEtapa = ultimaEtapa.getFechaLlegada();

                if (ultimaEtapa.getFechaLlegada() != null) {
                    if (ultimaEtapa.getFechaSalida() == null) {

                        fechaTotal = Math.abs(DAYS.between(fechaActual, fechaLlegadaEtapa));

                        if (fechaTotal > 7) {

                            envio.setEstado(Estado.Extraviado);

                            EnvioExtraviado envioExtraviado = new EnvioExtraviado();
                            envioExtraviado.setEnvioExtraviado(envio);
                            envioExtraviado.setFechaLlegada(fechaLlegadaEtapa);

                            repositorioEnvios.guardarExtraviado(envioExtraviado);
                            repositorioEnvios.actualizarEnvio(envio);

                        }

                    } else {

                        fechaTotal = Math.abs(DAYS.between(fechaActual, fechaSalidaEtapa));

                        if (fechaTotal > 7) {

                            envio.setEstado(Estado.Extraviado);

                            EnvioExtraviado envioExtraviado = new EnvioExtraviado();
                            envioExtraviado.setEnvioExtraviado(envio);
                            envioExtraviado.setFechaLlegada(fechaLlegadaEtapa);
                            envioExtraviado.setFechaSalida(fechaSalidaEtapa);

                            repositorioEnvios.guardarExtraviado(envioExtraviado);
                            repositorioEnvios.actualizarEnvio(envio);
                        }
                    }
                }

            }
        }
    }

    /**
     * Devuelve los envios extraviados
     * Se debería de hacer paginado pero no nos ha dado tiempo a implementarlo
     *
     * @return Lista de los envios extraviados
     */
    public List<Envio> verExtraviados() {

        List<Envio> extraviados = repositorioEnvios.obtenerEnviosExtraviados();
        List<Envio> enviosExtraviados = new ArrayList<>();

        extraviados.forEach(envio -> {
            enviosExtraviados.add(repositorioEnvios.devolverEnvio(envio.getLocalizador()));
        });
        
        return enviosExtraviados;
    }

    /**
     * Calcula el porcentaje de envios extraviados
     *
     * @param tipo Que parametro de busqueda quiere el usuario realizar
     * @return el importe del envio
     */
    public float porcentajeExtraviados(Rango tipo) {

        if (tipo.getRangoFecha() == null) {
            throw new AccionNoCorrespondiente();
        }

        List<Envio> envios = new ArrayList<>();

        int tamTodos = repositorioEnvios.cantidadEnvios();

        float porcentaje = 0;

        switch (tipo.getRangoFecha()) {

            case day:

                List<Envio> conFechaLlegada = repositorioEnvios.envioExtraviadoFechaLlegada();

                List<Envio> conFechaSalida = repositorioEnvios.envioExtraviadoFechaSalida();

                if (conFechaLlegada.isEmpty() && !conFechaSalida.isEmpty()) {

                    conFechaSalida.forEach(envio -> {
                        envios.add(repositorioEnvios.devolverEnvio(envio.getLocalizador()));
                    });

                } else if (!conFechaLlegada.isEmpty()) {

                    conFechaLlegada.forEach(envio -> {
                        envios.add(repositorioEnvios.devolverEnvio(envio.getLocalizador()));
                    });

                    conFechaSalida.forEach(envio -> {
                        envios.add(repositorioEnvios.devolverEnvio(envio.getLocalizador()));
                    });
                }

                int tam = envios.size();

                porcentaje = Precision.round((float) (tam * 100) / tamTodos, 2);

                return porcentaje;

            case month:

                List<Envio> conFechaLlegadaMes = repositorioEnvios.envioExtraviadoFechaLlegadaMes();

                List<Envio> conFechaSalidaMes = repositorioEnvios.envioExtraviadoFechaSalidaMes();

                if (conFechaLlegadaMes.isEmpty() && !conFechaSalidaMes.isEmpty()) {

                    conFechaSalidaMes.forEach(envio -> {
                        envios.add(repositorioEnvios.devolverEnvio(envio.getLocalizador()));
                    });

                } else if (!conFechaLlegadaMes.isEmpty()) {

                    conFechaLlegadaMes.forEach(envio -> {
                        envios.add(repositorioEnvios.devolverEnvio(envio.getLocalizador()));
                    });

                    conFechaSalidaMes.forEach(envio -> {
                        envios.add(repositorioEnvios.devolverEnvio(envio.getLocalizador()));
                    });
                }

                tam = envios.size();

                porcentaje = Precision.round((float) (tam * 100) / tamTodos, 2);

                return porcentaje;

            case year:

                List<Envio> conFechaLlegadaYear = repositorioEnvios.envioExtraviadoFechaLlegadaYear();

                List<Envio> conFechaSalidaYear = repositorioEnvios.envioExtraviadoFechaSalidaYear();

                if (conFechaLlegadaYear.isEmpty() && !conFechaSalidaYear.isEmpty()) {

                    conFechaSalidaYear.forEach(envio -> {
                        envios.add(repositorioEnvios.devolverEnvio(envio.getLocalizador()));
                    });

                } else if (!conFechaLlegadaYear.isEmpty()) {

                    conFechaLlegadaYear.forEach(envio -> {
                        envios.add(repositorioEnvios.devolverEnvio(envio.getLocalizador()));
                    });

                    conFechaSalidaYear.forEach(envio -> {
                        envios.add(repositorioEnvios.devolverEnvio(envio.getLocalizador()));
                    });
                }

                tam = envios.size();

                porcentaje = Precision.round((float) (tam * 100) / tamTodos, 2);

                return porcentaje;

            default:
                throw new AccionNoCorrespondiente();
        }
    }

    /**
     * Comprueba la posicion del envio en la lista
     *
     * @param envios lista de envios
     * @param localizador localizador del envio que se quiere buscar
     * @return la posicion del envio
     */
    public int buscarLocalizadorEnLista(List<Envio> envios, int localizador) {

        int c = 0;
        for (Envio envio : envios) {
            if (envio.getLocalizador() == localizador) {

                break;
            }
            c++;
        }
        return c;
    }
}
