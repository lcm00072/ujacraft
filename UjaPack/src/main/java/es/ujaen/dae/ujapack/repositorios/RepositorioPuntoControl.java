/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujapack.repositorios;

import es.ujaen.dae.ujapack.entidades.Centro;
import es.ujaen.dae.ujapack.entidades.Oficina;
import es.ujaen.dae.ujapack.entidades.Provincia;
import es.ujaen.dae.ujapack.entidades.PuntoControl;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author 1hlui
 */
@Repository
@Transactional
public class RepositorioPuntoControl {

    @PersistenceContext
    EntityManager em;

    public Optional<PuntoControl> buscar(int id) {
        return Optional.ofNullable(em.find(PuntoControl.class, id));
    }

    public int cantidadPuntos() {
        String selectFrom = "SELECT COUNT(id) FROM PuntoControl";
        Query query = em.createQuery(selectFrom);

        int total = Integer.parseInt(query.getSingleResult().toString());

        return total;
    }

    public void guardar(PuntoControl puntoControl) {
        em.persist(puntoControl);
    }

    public PuntoControl devolverPunto(int id) {
        return em.find(PuntoControl.class, id);
    }

//    public Centro tamañoConexiones(int id){
//    }
    public Optional<Centro> buscarCentro(int id) {
        return Optional.ofNullable(em.find(Centro.class, id));
    }

    public Centro devolverCentro(int id) {
        Centro centro = em.find(Centro.class, id);
        centro.getConexiones().size();
        return centro;
    }

    public Centro cargarConexiones(int id) {
        Centro centro = em.find(Centro.class, id);
        centro.getConexiones().size();
        return centro;
    }

    public void guardarCentro(Centro centro) {
        em.persist(centro);
    }

    public void nuevoCentro(Centro centro, Provincia provincia) {
        em.persist(centro);
    }

    public Optional<Provincia> buscarProvincia(String nombre) {
        return Optional.ofNullable(em.find(Provincia.class, nombre));
    }

    public void guardarProvincia(Provincia provincia) {
        em.persist(provincia);
    }

    public Provincia devolverProvincia(String nombre) {
        return em.find(Provincia.class, nombre);
    }

    public void guardarOficina(Oficina oficina) {
        em.persist(oficina);
    }

    public Oficina devolverOficina(int id) {
        return em.find(Oficina.class, id);
    }

    public void ActualizarCentro(Centro centro) {
        em.merge(centro);
    }

    public void nuevaConexion(Centro centro1, Centro centro2) {

        centro1 = em.merge(centro1);
        centro1.nuevaConexion(centro2);
    }

}
