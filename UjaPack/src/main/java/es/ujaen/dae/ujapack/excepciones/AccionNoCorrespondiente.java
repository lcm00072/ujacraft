/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.ujaen.dae.ujapack.excepciones;

/**
 * Excepción provocada para que no se entregue el envio
 * si no ha salido del ultimo punto de control
 * de la ruta
 * @author 1hlui
 */
public class AccionNoCorrespondiente extends RuntimeException {

    public AccionNoCorrespondiente() {
    }
}