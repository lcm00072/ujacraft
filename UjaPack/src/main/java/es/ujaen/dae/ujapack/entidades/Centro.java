/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujapack.entidades;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;

/**
 *
 * @author 1hlui
 */
@Entity
public class Centro extends PuntoControl{
 
    /** Nombre del centro logistico / Comunidad Autonoma */
    @NotBlank
    private String nombreCentro;
    
    /** Numero de la conexion del centro logistico */
    private int numero_con;

    /** Provincia sede del centro */
    private String sede;
    
    
    /** Lista de provincias del centro */
    @OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn
    private List<Provincia> provincias;
    
    /** Lista de conexiones del centro */
    
    @ManyToMany
    private List<Centro> conexiones;

    public Centro() {
        super();
    }
    
      public Centro(String nombreCentro, int numero_con, String sede, int id) {
        this.nombreCentro = nombreCentro;
        this.numero_con = numero_con;
        this.sede = sede;
        this.id = id;
        provincias = new ArrayList<>();
        conexiones = new ArrayList<>();
    }

    /**
     * @return the nombreCentro
     */
    public String getNombreCentro() {
        return nombreCentro;
    }

    /**
     * @return the numero_con
     */
    public int getNumero_con() {
        return numero_con;
    }

    /**
     * @return the sede
     */
    public String getSede() {
        return sede;
    }

    /**
     * @return the provincias
     */
    public List<Provincia> getProvincias() {
        return provincias;
    }

    /**
     * @return the conexiones
     */
    public List<Centro> getConexiones() {
        return conexiones;
    }

    /**
     * @param provincias the provincias to set
     */
    public void setProvincias(List<Provincia> provincias) {
        this.provincias = provincias;
    }

    /**
     * @param conexiones the conexiones to set
     */
    public void setConexiones(List<Centro> conexiones) {
        this.conexiones = conexiones;
    }

    /**
     * @param nombreCentro the nombreCentro to set
     */
    public void setNombreCentro(String nombreCentro) {
        this.nombreCentro = nombreCentro;
    }

    /**
     * @param numero_con the numero_con to set
     */
    public void setNumero_con(int numero_con) {
        this.numero_con = numero_con;
    }

    /**
     * @param sede the sede to set
     */
    public void setSede(String sede) {
        this.sede = sede;
    }

    public void nuevaConexion(Centro centro) {
        conexiones.add(centro);
    }
}
