/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujapack.entidades;


import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 *
 * @author 1hlui
 */
@Entity
public class Oficina extends PuntoControl {
    
    /** Provincia donde esta la oficina */  

    @OneToOne
    private Provincia localidad;

    public Oficina() {
        super();
    }
    
    public Oficina(Provincia nLocalidad) {
        this.localidad = nLocalidad;
    }

    /**
     * @return the localidad
     */
    public Provincia getLocalidad() {
        return localidad;
    }

    /**
     * @param localidad the localidad to set
     */
    public void setLocalidad(Provincia localidad) {
        this.localidad = localidad;
    }
    

}
