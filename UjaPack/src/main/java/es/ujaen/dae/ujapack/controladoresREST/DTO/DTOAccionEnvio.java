/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujapack.controladoresREST.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;

/**
 *
 * @author 1hlui
 */
public class DTOAccionEnvio {

    public enum Accion {
        salida,
        llegada,

    };

    int idPunto;

    Accion accion;

    @JsonFormat
    private LocalDateTime fecha;

    public DTOAccionEnvio(int idPunto, Accion accion, LocalDateTime fecha) {
        this.idPunto = idPunto;
        this.accion = accion;
        this.fecha = fecha;
    }

    public DTOAccionEnvio(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public DTOAccionEnvio(Accion accion) {
        this.accion = accion;
    }

    public int getIdPunto() {
        return idPunto;
    }

    public Accion getAccion() {
        return accion;
    }

    /**
     * @return the fecha
     */
    public LocalDateTime getFecha() {
        return fecha;
    }

}
