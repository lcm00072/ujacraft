/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujapack.entidades;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Size;


/**
 *
 * @author 1hlui
 */
@Entity
public class Provincia implements Serializable {

    /** Nombre de la provincia */
    @Id
    @Size(max=50)
    String n_Prov;

    public Provincia() {
    }

    
    public Provincia(String n_Prov) {
        this.n_Prov = n_Prov;
    }
    


    /**
     * @return the n_Prov
     */
    public String getN_Prov() {
        return n_Prov;
    }

    /**
     * @param n_Prov the n_Prov to set
     */
    public void setN_Prov(String n_Prov) {
        this.n_Prov = n_Prov;
    }
    
     

    
}
