/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujapack.controladoresREST;

import es.ujaen.dae.ujapack.controladoresREST.DTO.DTOAccionEnvio;
import es.ujaen.dae.ujapack.controladoresREST.DTO.DTOEnvio;
import es.ujaen.dae.ujapack.controladoresREST.DTO.DTOEnvioExtraviado;
import es.ujaen.dae.ujapack.controladoresREST.DTO.DTOEtapa;
import es.ujaen.dae.ujapack.controladoresREST.DTO.DTOPersona;
import es.ujaen.dae.ujapack.entidades.Envio;
import es.ujaen.dae.ujapack.entidades.Envio.Estado;
import es.ujaen.dae.ujapack.entidades.EnvioExtraviado;
import es.ujaen.dae.ujapack.entidades.Persona;
import es.ujaen.dae.ujapack.excepciones.AccionNoCorrespondiente;
import es.ujaen.dae.ujapack.excepciones.EnvioEnTransito;
import es.ujaen.dae.ujapack.excepciones.EnvioExtraviadoExcepcion;
import es.ujaen.dae.ujapack.excepciones.EnvioNoCreado;
import es.ujaen.dae.ujapack.excepciones.EnvioSinEtapas;
import es.ujaen.dae.ujapack.excepciones.PersonaNoEncontrada;
import es.ujaen.dae.ujapack.excepciones.PersonaYaGuardada;
import es.ujaen.dae.ujapack.excepciones.PuntoNoCorrespondiente;
import es.ujaen.dae.ujapack.servicios.ServicioUjaPack;
import es.ujaen.dae.ujapack.utilidades.Rango;
import es.ujaen.dae.ujapack.utilidades.Rango.RangoFecha;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author 1hlui
 */
@RestController
@RequestMapping("/ujapack")
public class ControladorREST {

    @Autowired
    ServicioUjaPack servicios;

    
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    public void handlerViolacionRestricciones(ConstraintViolationException e) {
//         return ResponseEntity.badRequest().body(e.getMessage());
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void handlerDataIntegrityViolationException(DataIntegrityViolationException e) {
//         return ResponseEntity.badRequest().body(e.getMessage());
    }

    @ExceptionHandler(EnvioNoCreado.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handlerEnvioNoCreado(EnvioNoCreado e) {
    }

    @ExceptionHandler(AccionNoCorrespondiente.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void handlerAccionNoCorrespondiente(AccionNoCorrespondiente e) {
    }

    /** Utilizado para comprobar si el usuario tiene permiso para hacer login **/
    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    void comprobarConexion() {

    }

    /**
     * Post para guardar personas
     *
     * @param DTOpersona de la persona a guardar
     * @return devuelve la respuesta de la solicitud
     */
    @PostMapping("/personas")
    ResponseEntity<DTOPersona> guardarPersona(@Valid @RequestBody DTOPersona DTOpersona) {

        try {

            /* Se comprueba si la localidad de la persona esta en la base de datos */
            boolean localidadExistente = servicios.comprobarLocalidadExistente(DTOpersona.aPersona());

            if (localidadExistente == false) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }

            Persona persona = servicios.guardarPersona(DTOpersona.aPersona());
            return ResponseEntity.status(HttpStatus.CREATED).body(new DTOPersona(persona));
        } catch (PersonaYaGuardada e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

    }

    /**
     * Post para crear un envio
     *
     * @param DTOenvio del envio para crear
     * @return devuelve la respuesta de la solicitud
     */
    @PostMapping("/envios")
    ResponseEntity<DTOEnvio> crearEnvio(@NotNull @RequestBody DTOEnvio DTOenvio) {

        try {

            Persona remitente = servicios.buscarPersona(DTOenvio.getRemitente());
            Persona destinatario = servicios.buscarPersona(DTOenvio.getDestinatario());

            Envio envio = servicios.crearEnvio(DTOenvio.getPeso(), DTOenvio.getDim(), remitente, destinatario);

            return ResponseEntity.status(HttpStatus.CREATED).body(new DTOEnvio(envio));
        } catch (PersonaNoEncontrada e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

    }

    /**
     * Put que sirve para notificar de la llegada o salida de un envio a un punto de control
     *
     * @param DTOAccionEnvio tipo de accion 
     * @param localizador del envio
     * @param idPunto del punto de control
     * @return devuelve la respuesta de la solicitud
     */
    @PutMapping("/envios/{localizador}/etapas/{idPunto}")
    ResponseEntity moverEnvio(@PathVariable int localizador, @PathVariable int idPunto, @RequestBody DTOAccionEnvio accionEnvio) throws InterruptedException {

        try {
            switch (accionEnvio.getAccion()) {
                case llegada:
                    servicios.llegadaEnvio(localizador, idPunto);
                    break;

                case salida:
                    servicios.salidaEnvio(localizador, idPunto);
                    break;

                default:
                    return ResponseEntity.badRequest().build();
            }
        } catch (EnvioExtraviadoExcepcion e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        } catch (PuntoNoCorrespondiente | AccionNoCorrespondiente e) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
        } catch (EnvioNoCreado e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        return ResponseEntity.ok().build();

    }

    /**
     * Put que sirve para registrar la entrega de un envio
     *
     * @param DTOAccionEnvio tipo de accion 
     * @param localizador del envio
     * @param idPunto del punto de control
     * @return devuelve la respuesta de la solicitud
     */
    @PutMapping("/envios/{localizador}/etapas/destino")
    ResponseEntity entregarEnvio(@PathVariable int localizador, @RequestBody DTOAccionEnvio accionEnvio) throws InterruptedException {

        try {

            servicios.entregarEnvio(localizador, accionEnvio.getFecha());

        } catch (EnvioNoCreado | EnvioSinEtapas e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (EnvioExtraviadoExcepcion e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        } catch (EnvioEnTransito e) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
        }

        return ResponseEntity.ok().build();

    }

    /**
     * Get para recibir un DTO del envio con sus datos
     *
     * @param localizador del envio
     * @return devuelve el DTO del envio
     */
    @GetMapping("/envios/{localizador}")
    ResponseEntity<DTOEnvio> verDatosEnvio(@PathVariable int localizador) {

        try {
            
            Envio envio = servicios.buscarEnvio(localizador);

            return ResponseEntity.status(HttpStatus.OK).body(new DTOEnvio(envio));

        } catch (EnvioNoCreado e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
    
    /**
     * Get para recibir un DTO del envio con su estado
     *
     * @param localizador del envio
     * @return devuelve el DTO del envio
     */
    @GetMapping("/envios/{localizador}/estado")
    ResponseEntity<DTOEnvio> verEstado(@PathVariable int localizador) {

        try {
            Estado estado = servicios.verEstado(localizador);
            Envio envio = servicios.buscarEnvio(localizador);

            return ResponseEntity.status(HttpStatus.OK).body(new DTOEnvio(localizador, estado, envio.getEntrega()));

        } catch (EnvioNoCreado e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    /**
     * Get para obtener las etapas de un envio
     *
     * @param localizador del envio
     * @return devuelve las etapas del envio
     */
    @GetMapping("/envios/{localizador}/etapas")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity<List<DTOEtapa>> verListado(@PathVariable int localizador) {

        try {
            ResponseEntity<List<DTOEtapa>> lista = ResponseEntity.status(HttpStatus.OK).body(servicios.verListado(localizador).stream()
                    .map(e -> new DTOEtapa(e)).collect(Collectors.toList()));

            if (servicios.verEstado(localizador).equals(Estado.Entregado)) {

                Envio envio = servicios.buscarEnvio(localizador);

                DTOEtapa nuevaEtapa = new DTOEtapa(envio.getEntrega(), envio.getDestinatario().getDireccion());

                lista.getBody().add(nuevaEtapa);

            }

            return (ResponseEntity<List<DTOEtapa>>) lista;
        } catch (EnvioNoCreado e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

    }

    /**
     * Get para ver los envios extraviados
     *
     * @return devuelve una lista de los envios extraviados
     */
    @GetMapping("/envios/extraviados")
    @ResponseStatus(HttpStatus.OK)
    List<DTOEnvio> verExtraviados() {

        List<DTOEnvio> DTOextraviados = servicios.verExtraviados().stream()
                .map(e -> new DTOEnvio(e)).collect(Collectors.toList());


        return DTOextraviados;

    }

    /**
     * Post para guardar un envio extraviado
     *
     * @param DTOEnvioExtraviado envio extraviado
     * @return devuelve la respuesta de la solicitud
     */
    @PostMapping("/envios/extraviados")
    ResponseEntity<DTOEnvioExtraviado> guardarExtraviado(@RequestBody DTOEnvioExtraviado DTOenvioExtraviado) {

        try {

            Envio envio = servicios.buscarEnvio(DTOenvioExtraviado.envioExtraviado);

            EnvioExtraviado envioExtraviado = new EnvioExtraviado(envio,
                    DTOenvioExtraviado.FechaLlegada, DTOenvioExtraviado.FechaSalida);

            servicios.guardarExtraviado(envioExtraviado);

            return ResponseEntity.status(HttpStatus.CREATED).body(new DTOEnvioExtraviado(envioExtraviado));
        } catch (PersonaNoEncontrada e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

    }

    /**
     * Get para obtener el porcentaje de extraviados segun un rango
     *
     * @param tipo de la restriccion
     * @return devuelve el porcentaje
     */
    @GetMapping("/envios/extraviados/porcentaje/{tipo}")
    @ResponseStatus(HttpStatus.OK)
    float porcentajeExtraviados(@PathVariable String tipo) {

        Rango tipoRango = new Rango(RangoFecha.valueOf(tipo));
        return (float) servicios.porcentajeExtraviados(tipoRango);
    }

}
