/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujapack.utilidades;

/**
 *
 * @author 1hlui
 */
public class Rango {
    
   public enum RangoFecha {
        day,
        month,
        year
    };

    public Rango() {
    }
    
   RangoFecha rangoFecha; 

    public Rango(RangoFecha rangoFecha) {
        this.rangoFecha = rangoFecha;
    }
    

    public RangoFecha getRangoFecha() {
        return rangoFecha;
    }

    public void setRangoFecha(RangoFecha rangoFecha) {
        this.rangoFecha = rangoFecha;
    }
   
   
}
